# Docker Comands information

## How to build the project images

### Processing Module 
To build one PM docker:
`docker build -f PM_dockerfile -t processing_module:version -t processing_module:latest .`

To save the image in tar format:
`docker save processing_module:latest > processingModule.tar`

### Rabbit MQ

To build rabbitmq image: 
`docker build -f rabbit_Dockerfile -t rabbitmq4brain:v0.1 -t rabbitmq4brain:latest .`

To save the image in tar format: 
`docker save rabbitmq4brain:latest > rabbitmq4brain.tar`

To run a rabbitMQ container:
`docker run -d -ti --name rabbitmq_test -p 8080:15672 -p 5000:5672 -p 5001:15674 rabbitmq4brain:latest`

### Elastic Search and Kibana

To build a elasticsearch and kibana image:
`docker build -f ElasticKibanServer -t elastic_kibana:latest -t elastic_kibana:vx.y .`

To save the image in tar format: 
`docker save elastic_kibana:latest > elastic_kibana.tar`

To run in dev mode:
* `docker run --rm=true -ti --name elastic_kibana_test -p 5003:5601 -p 5004:9200 elastic_kibana:latest ./runElasticSearch.sh`

In version v0.2(elastic 4.6):
* `/etc/elasticsearch` - Configuration files elasticsearch.yml and logging.yml.
* `/var/lib/elasticsearch/data` - The location of the data files of each index / shard allocated on the node.
* `/usr/share/elasticsearch/bin` - Binary scripts including elasticsearch to start a node.
* `/var/log/elasticsearch` - Log files location

more on https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-dir-layout.html

### Other comands

How to build an Image:
`docker build --pull -f <Dockerfile_name> -t <name_image>:<tag_version> [-t <name_image>:<tag_version> ...] PATH |URL | .`
*   `-f` - Dockerfile name
*   `-t` - Images tag
*   `--pull` - force to pull the latest version of image
*   `[]` - optional

How to save ( in .tar):
`docker save <image_name>:<version> > <file>.tar`

How to load (of .tar):
`docker load < <file>.tar`

How to run new conteiner:
`docker run [-d]  [--rm=true] -ti [--name <container_name>] [-p <local_port>:<container_port> [-p <local_port>:<container_port> ...]] <Image_name>[:<version>] [<start_comand>]`
*   `-d` - Run in Detached mode
*   `--rm=true` - Remove container when you stop the conteiner
*   `-t` - Have a terminal
*   `-i` - Keep STDIN open even if not attached
*   `-p`- Publish all exposed ports to the host interfaces
*   `[]` - optional

How to stop a container:
`docker stop -t [<time>]  <container> [ <container>...]`
*   `-t` - Seconds to wait for stop before killing it (default 10)

How to start a container:
`docker start  <container> [ <container>...]`
*   `<container>` can be id ou name

How to attach an running conteiner:
`docker attach   <container>`
*   `<container>` can be id ou name

### Images Versions

| Image Name | Version |
|------------|---------|
| rabbitmq4brain | V0.2 |
| processing_module | V0.5 |
| elastic_kibana | V0.4 |