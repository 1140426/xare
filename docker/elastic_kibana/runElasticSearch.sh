#!/bin/bash

################################################################################
#	This script inicialize the the elastic search and the kibana in the 	   #
#	docker contheiner.														   #
################################################################################

# run elasticsearch
service elasticsearch start

# run kibana
service kibana start
