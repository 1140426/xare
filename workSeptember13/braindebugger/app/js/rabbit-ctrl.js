/**
 * Created by xarevision on 20-04-2016.
 */

(function(){

    var controller = angular.module('debugger.controllers.rabbit', []);
    controller.controller("RabbitMQCtrl", ["$rootScope", '$scope', "RabbitMQClient" , function($rootScope, $scope, RabbitMQClient) {

        var rabbitConfig = {
            user: getUrlParameter("user"),
            pass: getUrlParameter("pass"),
            host: getUrlParameter("host"),
            //TODO - ver porta
            port: getUrlParameter("port")
        };

    	reconnectTimeOut(RabbitMQClient, rabbitConfig);

		var publishingqueue = "layer1";

		var conn = RabbitMQClient.$get(rabbitConfig.user, rabbitConfig.pass, publishingqueue, rabbitConfig.host, rabbitConfig.port, "");
		
		conn.connect();

		//Set up buttons
		fillTokens();
		
		$("#SendRequest").click(function()
		{		
			var message = $("#inputText").val();
			sendJSON(message, 'layer1', conn);
			$('#clear_ta_all').click();
			start = performance.now();
		});

        // ligar ao rabbit e configurar eventos
        var events = {
           layersu: "RabbitEvent.ON_DEBUGGER_MESSAGE"
		}

        var debugLayer6 = RabbitMQClient.$get(rabbitConfig.user, rabbitConfig.pass, "layer2", rabbitConfig.host, rabbitConfig.port, events.layersu);

		debugLayer6.connect();
		// eventos para as mensagens do rabbit

		var root = $rootScope;

		onDebuggerMessage(root, events.layer2);

    }]);

})();
