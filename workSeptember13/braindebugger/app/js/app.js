angular.module('debugger-brain', ['ngRoute', 'debugger.services.rabbitmq', 'debugger.controllers.rabbit'])
    .config(["$routeProvider", function ($routeProvider) {
      $routeProvider
          .when('/debug', {
            templateUrl: 'templates/debug.html',
            controller : "RabbitMQCtrl"
          })
          .otherwise({redirectTo: '/debug'});
    }])
    // what services / modules we need to run when starting the app
    .run([function() {
      console.log("Application [INITIALIZATION SEQUENCE] Angular run starting");
    }]);

