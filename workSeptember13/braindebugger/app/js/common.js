
var start = getTime();
var end   = getTime();

function reconnectTimeOut(RabbitMQClient, rabbitConfig)
{
	setTimeout(function() 
	{
    	reconnect(RabbitMQClient, rabbitConfig);
	}, 2000);

}

function reconnect(RabbitMQClient, rabbitConfig)
{
		// ligar ao rabbit e configurar eventos
        var events = {
            layer1: "RabbitEvent.ON_LAYER1_MESSAGE",
            layer2: "RabbitEvent.ON_DEBUGGER_MESSAGE"
		}

    var rabbitConfig = rabbitConfig;

	var publishingqueue = "layer1";

	var conn 			= RabbitMQClient.$get(rabbitConfig.user, rabbitConfig.pass, publishingqueue, rabbitConfig.host, rabbitConfig.port, "");
	
	conn.connect();

    var debugLayer1 = RabbitMQClient.$get(rabbitConfig.user, rabbitConfig.pass, "layer1", rabbitConfig.host, rabbitConfig.port, events.layer1);
    var debugLayer6 = RabbitMQClient.$get(rabbitConfig.user, rabbitConfig.pass, "layer2", rabbitConfig.host, rabbitConfig.port, events.layersu);

    debugLayer1.connect();
    debugLayer6.connect();
}

function getTextRoutingKey()
{
	var routing_key = $("#routing_key_s").val();

	if(routing_key==undefined || routing_key=="")
		routing_key = "debug";

	return routing_key;
}

function getTime()
{
	return performance.now();
//	return (new Date()).getTime() / 1000.0 ;
}

function sendJSON(text, exchange, conn)
{
	var input 			= text;
	//var time 			= getTime();
	//var token 			= getTextRoutingKey();


	// for (var i = 0; i < input.length; i++)
	// {
	// 	var message   = input[i]
    //
	// 	var data      = message.data;
    //
	// 	// 1.00, 0.00, 0.00, 1.00, 1.00
	// 	var subjective_opinion         = new Object();
	// 	subjective_opinion.belief      = 1.0;
	// 	subjective_opinion.disbelief   = 0.0;
	// 	subjective_opinion.uncertainty = 0.0;
	// 	subjective_opinion.atomicity   = 1.0;
	// 	subjective_opinion.expectation = 1.0;
    //
	// 	data.subjective_opinion        = subjective_opinion;
    //
	// 	message.data      = data; //eu sei la sei la eu sei la sei la
	// 	message.request   = message.request + "-" + time + "";
	// 	message.processor = "browser";
	// 	message.routing   = "BROWSER";
	// 	message.token     = token;
    //
	// 	var message_jason = JSON.stringify(message);
    //
	// 	conn.send(input, layer, getTextRoutingKey());
	// }

	conn.send(input, exchange, getTextRoutingKey());

	start = getTime();

}

function onDebuggerMessage(rootScope, layerevent)
{
	rootScope.$on(layerevent, function (event, args) 
	{
		// var time = getTime() - start;
        //
		// var message = JSON.parse(args.message);
        // var t       = $('#ta_layer4-5').DataTable();
        // var bdua    = getBDUA(message);
		// var routing = getRouting(message.routing);
		// var token = getSelectedRoutingKey();
        //
		// if(token !== null && (message.token == token || token == 'todos'))
		// {
		// 	t.row.add([
		// 		message.data.item,
		// 		message.data.confidence, bdua,
		// 		message.data.original_word,
		// 		message.processor,
		// 		message.data.type,
		// 		time,
		// 		message.timestamp,
		// 		routing
		// 	]).draw(false);
		//
		// 	addItemToResultsList(message);
		// }
		addItemToResultsList(message);
	});
}
	

function addItemToResultsList(item_message)
{
	var item 		= item_message.data.item;
	var confidence  = item_message.data.confidence;
	var type        = item_message.data.type;
	var routing     = getRouting(item_message.routing);
	var ean_list    = item_message.data.ean_list;
	var bdua 		= getBDUA(item_message);
	var request     = item_message.request;
	var brand_name  = item_message.data.brand_name;

	request = request.replace(/\./g,'');

	confidence      = Math.round(confidence * 100) / 100 ;

	c_item          =  encodeURIComponent(escape(item));

	if (item != "pong")
	{
		if (!hasRequestQueue(request))
			createQueue(request, item_message.data.original_word);

		addItem(item, ean_list, confidence, request, brand_name);	    
	}

	addItem()

}

function addItem(item, ean_list, confidence, request, brand_name)
{
	var p1 = $('<p/>').text(item);
	var p2 = $('<p/>').text(ean_list).attr('style', "font-size:8px");
	var p3 = $('<p/>').text(confidence);
	var p4 = $('<p/>').text(request).attr('style', "font-size:8px");
	var p5 = $('<p/>').text(brand_name).attr('style', "font-size:9px");
    
    var newli = $('<li/>').addClass('item').attr('conf', confidence).attr("ID", ean_list);
    newli.append(p1); newli.append(p2); newli.append(p5); newli.append(p4); newli.append(p3);

    var request_id = '#' + request;

	var ul = $(request_id);

	var isrepeated = isRepeated(ean_list, request);

	if(isrepeated)
	{
		var old = document.getElementById(ean_list);
		document.getElementById(request).removeChild(old);
	} 

	ul.append(newli);

	var li = ul.children('li');

   	li.detach().sort(function(a, b) 
	{
		 return $(b).attr('conf') - $(a).attr('conf');
	});

	ul.append(li);
	//$('#results_list').append(ul);
}


function createQueue(request, original_word)
{
	var newul = $('<ul/>', { id: request})
	var name  = $('<b/>').text('Request: ' +  request + " | " + original_word);
	$('#results_list').append(name);
	$('#results_list').append(newul);
}

function hasRequestQueue(request)
{
	return document.getElementById(request) !== null;
}

function isRepeated(ean_list, request)
{
	if(hasRequestQueue(request))
	{

		var lis = document.getElementById(request).getElementsByTagName("li");

		for(var i = 0; i < lis.length; i++)
		{
			var item = lis[i].id;

			if (ean_list == item)
				return true;
		}	
	}

	return false;
}

function getUrlParameter(sParam)
{
	var results = new RegExp('[\?&]' + sParam + '=([^&#]*)').exec(window.location.href);
	if (results==null){
	   return null;
	}
	else{
	   return results[1] || 0;
	}
}