import sys

sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule


class RotatorProcessingModule(ProcessingModule):
    def __init__(self):
        super(RotatorProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(RotatorProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(RotatorProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(RotatorProcessingModule, self).setMemory(key, value)


module = RotatorProcessingModule()
