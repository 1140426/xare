import sys

sys.path.append("../../")
from PModule.ProcessingModule import ProcessingModule


class TestProcessingModule(ProcessingModule):
    def __init__(self):
        super(TestProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(TestProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(TestProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(TestProcessingModule, self).setMemory(key, value)


module = TestProcessingModule()
