import Image
import ImageFilter
import os
from clint.textui import colored
from clint.textui import puts

import generator_config_file
from src.iofile.fileio import FileIO
from src.picture.picture import Picture


class AutomaticModule(object):
    def run(self, blur, pathImages, pathDest):
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)
                imblur = im.convert('L').filter(ImageFilter.GaussianBlur(float(blur)))
                imblur.save(pathDest + 'Automatic/{}_{}{}'.format(fn, blur, fext))
                im.save(pathDest + 'Automatic/{}{}'.format(fn, fext))
                print "Sucess : ", im
                print "Sucess : ", imblur
                transpose = im.transpose(Image.FLIP_LEFT_RIGHT)
                transposeBlur = imblur.transpose(Image.FLIP_LEFT_RIGHT)
                transposeBlur.save(pathDest + 'Automatic/{}_T{}{}'.format(fn, blur, fext))
                transpose.save(pathDest + 'Automatic/{}T{}'.format(fn, fext))
                print "Transpose Sucess!"
                rotation = 10
                while rotation < 181:
                    out_1 = im.rotate(int(rotation), expand=True)
                    out_2 = imblur.rotate(int(rotation), expand=True)
                    out_2.save(pathDest + 'Automatic/{}_{}_{}{}'.format(fn, blur, rotation, fext))
                    out_1.save(pathDest + 'Automatic/{}_{}{}'.format(fn, rotation, fext))
                    print "Sucess Rotation:", rotation, "for ->", im
                    print "Sucess Rotation:", rotation, "for ->", imblur
                    rotation += 10

    pass

    def generator(self, pathImages, pathDest):
        fileio = FileIO()
        for f in os.listdir(pathImages):
            if f.endswith(generator_config_file.IMAGES_EXTENSION):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)

                # Nome imagem original e guarda
                imSaveName = "{}{}".format(fn, fext)
                fileio.saveImage(im, imSaveName, pathDest, fn)
                fileio.saveFile(fn, imSaveName, 0, 0, False, pathImages, pathDest, fn)

                # Apply Transpose effect
                transpose = im.transpose(Image.FLIP_LEFT_RIGHT)
                imTranposeName = "{}_T{}".format(fn, fext)

                # Save
                fileio.saveImage(transpose, imTranposeName, pathDest, fn)
                fileio.saveFile(fn, imTranposeName, 0, 0, True, pathImages, pathDest, fn)
                puts(colored.white("Successfully Transpose for image: " + imTranposeName))

                rotation = generator_config_file.ROTATION
                while rotation <= generator_config_file.ROTATION_MAX:
                    # Rotate original
                    out_1 = im.rotate(int(rotation), expand=True)
                    name1 = "{}_{}{}".format(fn, rotation, fext)

                    # SAVE
                    fileio.saveImage(out_1, name1, pathDest, fn)
                    fileio.saveFile(fn, name1, 0, rotation, False, pathImages, pathDest, fn)
                    puts(colored.cyan("Successfully Rotation: " + str(rotation) + " for image: " + name1))

                    # Next Rotation
                    rotation += generator_config_file.ROTATION

                for blur in range(1, generator_config_file.BLUR_MAX, 1):
                    # Apply Blur
                    imblur = im.convert('RGB').filter(ImageFilter.GaussianBlur(float(blur)))

                    # Build image name
                    imBlurSaveName = "{}_{}{}".format(fn, blur, fext)

                    # Save image
                    fileio.saveImage(imblur, imBlurSaveName, pathDest, fn)
                    fileio.saveFile(fn, imBlurSaveName, blur, 0, False, pathImages, pathDest, fn)

                    puts(colored.yellow("Successfully applied Blur: " + str(blur) + " for image: " + fn + fext))

                    # Apply Transpose effect
                    transposeBlur = imblur.transpose(Image.FLIP_LEFT_RIGHT)

                    # Build image name
                    imBlurTransposeName = "{}_T{}{}".format(fn, blur, fext)

                    # Save Image
                    fileio.saveImage(transposeBlur, imBlurTransposeName, pathDest, fn)
                    fileio.saveFile(fn, imBlurTransposeName, blur, 0, True, pathImages, pathDest, fn)
                    puts(colored.cyan("Successfully Transpose for image: " + imBlurTransposeName))

                    rotation = generator_config_file.ROTATION
                    while rotation <= generator_config_file.ROTATION_MAX:
                        # Rotate Original with Blur
                        out_2 = imblur.rotate(int(rotation), expand=True)

                        # Build names
                        name2 = "{}_{}_{}{}".format(fn, blur, rotation, fext)

                        # Save Images
                        fileio.saveImage(out_2, name2, pathDest, fn)
                        fileio.saveFile(fn, name2, blur, rotation, False, pathImages, pathDest, fn)
                        puts(colored.white("Successfully Rotation: " + str(rotation) + " for image: " + name2))
                        # Next Rotation
                        rotation += generator_config_file.ROTATION

    pass

    def generatorB(self, pathImages, pathDest):
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                picture1 = Picture(pathImages, pathDest, f, "Automatic/")
                picture2 = Picture(pathImages, pathDest, f, "Automatic/")

                picture1.save()

                picture1.transposeImage()

                # for Picture 2
                for blur in range(1, 5, 1):
                    # Apply Blur
                    picture2.applyBlur(blur)

                    # Save image
                    picture2.save(originalName, pathImages, pathDest, "Automatic/")

                    puts(
                        colored.yellow("Successfully applied Blur: " + str(blur) + " for image: " + picture2.getName()))

                    # Apply Transpose effect
                    # picture2.transposeImage()
                    # picture2.save(originalName,pathImages,pathDest,"Automatic/")

                    # puts(colored.white("Successfully Transpose for image: " + imTranposeName))
                    # puts(colored.cyan("Successfully Transpose for image: " + picture2.getName()))
                    rotation = 10
                    while rotation < 181:
                        # Rotate Original with Blur
                        nameaux = picture2.getName()
                        picture2.rotateImage(rotation)

                        # picture2.save(originalName, pathImages, pathDest, "Automatic/")

                        # puts(colored.cyan("Successfully Rotation: " + str(rotation) + " for image: " + name1))
                        puts(colored.white(
                            "Successfully Rotation: " + str(rotation) + " for image: " + picture2.getName()))

                        # Next Rotation
                        rotation += 10
                        picture2.setName(nameaux)
                    picture2.setName(originalName)

    pass
