import json
import os


class FileIO(object):
    def saveImage(self, image, imageName, pathDest, folderName):
        if not os.path.exists(pathDest + folderName):
            os.makedirs(pathDest + folderName)
        image.save(pathDest + folderName + "/" + imageName)

    pass

    def saveFile(self, originalName, imageName, blur, rotation, transpose, pathOring, pathDest, folderName):
        if not os.path.exists(pathDest + folderName):
            os.makedirs(pathDest + folderName)
        with open(pathOring + originalName + '.json', 'r+') as f:
            data = json.load(f)
            f.close()

        data["name"] = imageName
        data["blur"] = str(blur)
        data["rotation"] = str(rotation)
        data["transposed"] = str(transpose)

        fn, fext = os.path.splitext(imageName)

        jsonFile = open(pathDest + folderName + "/" + str(fn) + ".json", "w+")
        jsonFile.write(json.dumps(data))
        jsonFile.close()

    pass
