from HocrFileParser import HocrFileParser


class HocrConfidenceAnalyzer(object):
    # hocr to analyze file path
    hocrFilePath = None

    # dictionary containing read values and their confidence
    dictionary = None

    # initializer
    def __init__(self, hocrFilePath):
        self.hocrFilePath = hocrFilePath

    #instantiates a parser and feeds it (in order to read)
    def processConfidencePerReadResult(self):

        # instantiate the parser and fed it HTML
        with open(self.hocrFilePath, 'r') as hocrFile:
            data = hocrFile.read()  # read file

        parser = HocrFileParser()
        parser.feed(data)

        self.dictionary = self.__fixErrosInConfidenceDetection(parser.readDictionary)

        return self.dictionary

    #returns the result with higher confidence
    def resultWithHigherConfidence(self):
        resultWithHigherConfidence = []

        auxBiggestConfidence = 0
        for key, value in self.dictionary.iteritems():
            if (value[1][0] > auxBiggestConfidence): #bigger confidence detected
                auxBiggestConfidence = value[1][0]
                resultWithHigherConfidence = []
                resultWithHigherConfidence.append(value[0][0]) #read result
                resultWithHigherConfidence.append(value[1][0]) #confidence

        return resultWithHigherConfidence

    # remove euros, commas and adjust calculation when whitespaces are detected
    def __fixErrosInConfidenceDetection(self, confidenceDictionary):
        # NB: dictionary format:
        # {key : [[detected, strings, here], [confidence, per, string]]}

        newDictionary = dict(confidenceDictionary)
        auxCounter = 0

        for key, value in newDictionary.iteritems():
            if (len(value[0]) > 1):  # if more than one string detected
                # iterate the list
                for i in xrange(len(value[0])):
                    auxCounter += int(value[1][i])

                # there is one whitespace for each two items in the list and it's weigth is 0 <- convention
                auxCounter = int(auxCounter / int((len(value[0]) + (len(value[0]) - 1))))
                newValueList = []
                newStringsList = ''.join(value[0])
                newValueList.append([newStringsList])
                newValueList.append([auxCounter])

                newDictionary[key] = newValueList
                auxCounter = 0

            #remove euro symbol and other non ascii chars
            value[0][0] = self.__removeNonAsciiChar(value[0][0])
            if(len(value[0][0]) == 0):
                newDictionary = self.__removeKeyFromDictionary(newDictionary, key)

            #remove commas from read result
            if("," in value[0][0]):
                value[0][0] = self.__cleanPriceValue(value[0][0])

        return newDictionary

    #removes key from the sent dictionary
    def __removeKeyFromDictionary(self, dictionary, key):
        newDictionary = dict(dictionary)
        del newDictionary[key]
        return newDictionary

    #removes non ascii chars from the sent string
    def __removeNonAsciiChar(self, str):
        return "".join(i for i in str if ord(i) < 128)

    #Removes duplicated commas and whitespaces
    def __cleanPriceValue(self, price):
        cleanPrice = []

        for c in price:
            if (c == ","):
                if (c not in cleanPrice): #only one comma per price
                    cleanPrice.append(c)
            else:
                if(c != " "): #if not a white space
                    cleanPrice.append(c) #not a comma

        newPrice =  ''.join(cleanPrice)

        #remove commas at the end and at the beggining of the price
        if (newPrice.index(',') == 0 or newPrice.index(',') == len(newPrice) - 1 ):
            newPrice = newPrice.replace(',', '')
            newPrice = newPrice.strip()

        return newPrice

"""
if __name__ == '__main__':
    parser = HocrConfidenceAnalyzer('../../TestingImages/resultadoxhtml.hocr')
    bra = parser.processConfidencePerReadResult()

    biggestResult = parser.resultWithBiggestConfidence()
    print biggestResult[0]
"""