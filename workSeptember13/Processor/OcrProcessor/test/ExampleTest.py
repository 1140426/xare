import unittest

class ExampleTestClass(unittest.TestCase):

    #instantiates a GenericBoundingBoxCreator with given values
    def setUp(self):
        self.number = 2

    def tearDown(self):
        self.number = None

    def test_true(self):
        self.assertEqual(self.number,2,
                         'message shown if the assertion failed')

    #def test_false(self):
    #    self.assertEqual(self.number, 33,
    #                     'different numbers')