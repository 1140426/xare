import sys

sys.path.append("../../../")

import unittest
from Processor.OcrProcessor.GenericBoundingBoxCreator import GenericBoundingBoxCreator
from Processor.OcrProcessor.JSONReader import JSONfileReader
import Processor.OcrProcessor.properties as properties

class GenericBoundingBoxCreatorTest(unittest.TestCase):

    JSON_IN = '{"barcode":"3219110333596","image":"testPriceTag.png","label_type":"JumboA","lower_point":[1470,694],"upper_point":[1312,137]}'

    EUROS_EXPECTED_RESULT = '123 326 826 312 description'
    CENTS_EXPECTED_RESULT = '991 473 246 165 description'
    COMPLEX_EXPECTED_RESULT = '123 326 1112 312 description'

    # tests the creation of a simple cents bounding box for the testing image
    def test_simpleCentsBoundingBoxAnalysis(self):
        jsonAsDictionary = self.__readJSON()
        simpleCentsBoundingBoxCreator = GenericBoundingBoxCreator(jsonAsDictionary,
                                                                  properties.SIMPLE_CENTS_BB_Y_PERCENTAGE_BARCODE,
                                                                  properties.SIMPLE_CENTS_BB_X_PERCENTAGE_BARCODE,
                                                                  properties.SIMPLE_CENTS_LIMIT_PERCENTAGE_BARCODE)
        simpleCentsBoundingBoxCreator.calculateBoundingBox()
        centsBBdescription = simpleCentsBoundingBoxCreator.generateBoundingBoxDescription()

        self.assertEqual(centsBBdescription, self.CENTS_EXPECTED_RESULT, "cents bounding box not correct!!")


    # tests the creation of a simple euros bounding box for the testing image
    def test_simpleEurosBoundingBoxAnalysis(self):
        jsonAsDictionary = self.__readJSON()
        simpleEurosBoundingBoxCreator = GenericBoundingBoxCreator(jsonAsDictionary,
                                                                  properties.SIMPLE_EUROS_BB_Y_PERCENTAGE_BARCODE,
                                                                  properties.SIMPLE_EUROS_BB_X_PERCENTAGE_BARCODE,
                                                                  properties.SIMPLE_EUROS_LIMIT_PERCENTAGE_BARCODE)
        simpleEurosBoundingBoxCreator.calculateBoundingBox()
        eurosBBdescription = simpleEurosBoundingBoxCreator.generateBoundingBoxDescription()

        self.assertEqual(eurosBBdescription, self.EUROS_EXPECTED_RESULT, "euros bounding box not correct!!")


    # tests the creation of a complex bounding box for the testing image
    def test_complexBoundingBoxAnalysis(self):
        jsonAsDictionary = self.__readJSON()
        complexBoundingBoxCreator = GenericBoundingBoxCreator(jsonAsDictionary,
                                                              properties.COMPLEX_BB_Y_PERCENTAGE_BARCODE,
                                                              properties.COMPLEX_BB_X_PERCENTAGE_BARCODE,
                                                              properties.COMPLEX_LIMIT_PERCENTAGE_BARCODE)

        complexBoundingBoxCreator.calculateBoundingBox()
        complexBBdescription = complexBoundingBoxCreator.generateBoundingBoxDescription()

        self.assertEqual(complexBBdescription, self.COMPLEX_EXPECTED_RESULT,
                         "complex analysis\' bounding box not correct!!")


    # Reads JSON content in the JSON string used and returns it as a dictionary.
    def __readJSON(self):
        jsonParser = JSONfileReader(self.JSON_IN)

        readContent = {properties.DICT_BARCODE_TOP_LEFT_X: jsonParser.readTopLeftCornerXposition(),
                       properties.DICT_BARCODE_TOP_LEFT_Y: jsonParser.readTopLeftCornerYposition(),
                       properties.DICT_BARCODE_BOTTOM_RIGHT_X: jsonParser.readBottomRightCornerXposition(),
                       properties.DICT_BARCODE_BOTTOM_RIGHT_Y: jsonParser.readBottomRightCornerYposition(),
                       properties.DICT_BARCODE_IMAGE: jsonParser.readImagePath()}

        return readContent