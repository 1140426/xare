import json
import sys
sys.path.append("../")
from Processor.Processor import Processor


class JudgmentProcessor(Processor):

    processingModuleInterface = None

    resultList = []

    dictOut = {}

    def __init__(self, processingModuleInterface):
        self.processingModuleInterface = processingModuleInterface


    def process(self, jsonin):
        if(len(self.resultList) == 0):
            self.resultList.append(jsonin)
        else:
            self.resultList.append(jsonin)
            print(self.resultList)
            self.processingModuleInterface.send(jsonin, json.load(self.resultList))

