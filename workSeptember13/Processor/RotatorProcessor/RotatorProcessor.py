import json
import sys
import cv2
import numpy as np
import math
import itertools
import operator

sys.path.append("../")

from SimpleCV import Image

from Processor.Processor import Processor


class RotatorProcessor(Processor):
    JSON_IMAGE = "image"
    JSON_LABEL_TYPE = "label_type"

    def __init__(self, processingModuleInterface):
        super(RotatorProcessor, self).__init__(processingModuleInterface)

    def process(self, jsonIn):
        """This function receives a json string as parameter and tries decode the barcode
        from the image whose path is inside the json file"""

        # create a json objects with the json string received by parameter
        jsonData = json.loads(jsonIn)

        # find the image inside the json object
        imagePath = str(jsonData[self.JSON_IMAGE])

        # opens the image to process
        img = cv2.imread(imagePath)


        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray, 50, 150, apertureSize=3)
        minLineLength = 100
        maxLineGap = 10
        lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)

        list = []

        for x1, y1, x2, y2 in lines[0]:

            ca = abs(x1 - x2)
            co = abs(y1 - y2)
            h = math.hypot(co, ca)
            div = ca / h
            angle = math.degrees(math.acos(div))
            if (x1 < x2 and y1 > y2):
                angle = -angle
            elif (x1 < x2 and y1 > y2):
                angle = -(180 - angle)
            elif (x1 > x2 and y1 < y2):
                angle = -(270 - angle)

            list.append(int(angle))


        def most_common(L):
            # get an iterable of (item, iterable) pairs
            SL = sorted((x, i) for i, x in enumerate(L))
            # print 'SL:', SL
            groups = itertools.groupby(SL, key=operator.itemgetter(0))

            # auxiliary function to get "quality" for an item
            def _auxfun(g):
                item, iterable = g
                count = 0
                min_index = len(L)
                for _, where in iterable:
                    count += 1
                    min_index = min(min_index, where)
                # print 'item %r, count %r, minind %r' % (item, count, min_index)
                return count, -min_index

            # pick the highest-count/earliest item
            return max(groups, key=_auxfun)[0]

        angleFinal = most_common(list)

        rows, cols = img.shape[:2]

        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angleFinal, 1)
        rotatedImg = cv2.warpAffine(img, M, (cols, rows))

        cv2.imwrite(imagePath, rotatedImg)


        # create a json output with the image path and the label type received
        jsonOut = {self.JSON_IMAGE: imagePath, self.JSON_LABEL_TYPE: str(jsonData[self.JSON_LABEL_TYPE])}

        jsonOut = json.loads(jsonOut)
        jsonOut["deviceID"] = jsonData["deviceID"]
        # send the jsonIn and jsonOut to the next layer
        self.processingModuleInterface.send(jsonIn, json.dumps(jsonOut))