from RabbitMQReceiver import RabbitMQReceiver
from RabbitMQSender import RabbitMQSender
from BlockingRabbitReceiver import BlockingRabbitReceiver
from BlockingRabbitSender import BlockingRabbitSender


class RabbitMQFactory(object):
    def getReceiverBroker(self, config, processor):
        return BlockingRabbitReceiver(config=config, processor=processor)

    def getSenderBroker(self, config):
        return BlockingRabbitSender(config=config)