from Rabbit.RabbitMQFactory import RabbitMQFactory
from Stdio.StandardIOFactory import StandardIOFactory


class BrainChannelFactory(object):
    @staticmethod
    def get_factory(factory):
        '''Abstract factory to get instances of concrete implementation.

        :param factory:
        :return Broker Factory:

        '''
        if factory == 'rabbitmq':
            return RabbitMQFactory()
        if factory == 'standardio':
            return StandardIOFactory()
        raise TypeError('Unknown Broker Factory')