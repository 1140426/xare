import ConfigParser
import logging
import os

class Config(object):

    loglocation = "/var/log/xarevision/config.log"

    def __init__(self, cfgfile):
        self.cfgfile = cfgfile
        self.config = self.readConfig()

    def ensureLocation(self):
        if not os.path.isfile(self.cfgfile):
            return False


    def readConfig(self):

        filename = self.cfgfile
        config = ConfigParser.ConfigParser()

        with open(filename, 'r') as configfile:
            config.readfp(configfile)

        return config


    def getConfigValue(self, config_name):

        try:
            return self.config.get("config", config_name)
        except Exception as e:
            self.LogError("Error getting configuration {0}.".format(config_name), e)

        return None

    def LogError(message, e):

        logging.basicConfig(filename=Config.loglocation,
                            level=logging.ERROR,
                            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

        text = "{0} | {1}".format(message, repr(e))
        logger = logging.getLogger("CONFIG")
        logger.error(text)

        print text
        return
