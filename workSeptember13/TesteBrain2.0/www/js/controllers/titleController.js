/**
 * Created by riscas on 8/24/16.
 */

var app=angular.module('ionicTitle', [])

app.controller('titleCtrl',function ($scope, $cordovaDevice, $cordovaBarcodeScanner, $cordovaCamera, $timeout, $cordovaFileTransfer){
    $scope.picturePath=undefined;

    $scope.server = "http://xareserver.dyndns.org:1024/~riscas/ionic_upload/upload.php";

    $scope.retries = 0;
    $scope.images = [];
    $scope.photoStatus = "Just Take a photo ";


    $scope.takePhoto = function () {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false,
            targetWidth: $scope.phtWidth,
            targetHeight: $scope.phtHeight,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            correctOrientation:true,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.pictureUrl = "data:image/jpeg;base64," + imageData;
            $scope.picturePath = imageData;

        }, function (err) {
            // An error occured. Show a message to the user
        });
    }

    $scope.choosePhoto = function () {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            targetWidth: $scope.phtWidth,
            targetHeight: $scope.phtHeight,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            correctOrientation:true,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.pictureUrl = "data:image/jpeg;base64," + imageData;
            $scope.picturePath = imageData;


        }, function (err) {
            // An error occured. Show a message to the user
        });
    }

    $scope.sendPicture = function () {
        //if the user took a photo
        if ($scope.picturePath != "") {


            //gets the image filename
            $scope.fileName = $scope.picturePath.substr($scope.picturePath.lastIndexOf('/') + 1);


            //server options
            var server = $scope.server;

            //set the filepath
            var filePath = $scope.picturePath;

            //get the deviceID
            $scope.uuidTeste = $cordovaDevice.getUUID();


            //additional parameters if need, just saving the code for later use
            var params = new Object();
            params.deviceId = $scope.uuidTeste;



            var options = {
                fileKey: "uploadedfile",
                fileName: filePath.substr(filePath.lastIndexOf('/') + 1),
                chunkedMode: false,
                mimeType: "image/jpg",

            };


            options.params = params;

            //try uploading
            tryUpload();


            //function that try the upload
            // I made a function, so Ican call it later one more time if it fails
            // *I read somewhere that it's a Cordova bug and trying again "fix" it
            function tryUpload() {

                //the action / the magic
                $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function (result) {
                        uploadSucces(result);
                    }, function (err) {
                        uploadError(err);
                    });

            }

            //if success
            var uploadSucces = function (result) {
               // var resulted =result.substr(result.IndexOf('responseCode') + 1,3)
              $scope.picturePath=undefined;
                $scope.pictureUrl=undefined;
                $scope.photoStatus="Image Send";
            }

            //if fail, try one more time
            var uploadError = function (error) {

                if ($scope.retries == 0) {
                    //try one more time
                    $scope.retries++;
                    setTimeout(function () {
                        tryUpload();
                    }, 1000)
                } else {

                    //reset retries
                    $scope.retries = 0;
                    $scope.picturePath=undefined;
                    $scope.pictureUrl=undefined;
                    $scope.photoStatus="Image not send";
                }
            }
        }
    }



});