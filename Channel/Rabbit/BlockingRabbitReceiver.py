import pika
from functools import partial

from Channel.Receiver import Receiver


class BlockingRabbitReceiver(Receiver):


    def __init__(self,config,processor):

        self.processor = processor

        self.mq_user = config.getConfigValue('mq_user')
        self.mq_pass = config.getConfigValue('mq_pass')
        self.mq_host = config.getConfigValue('mq_host')
        self.mq_port = config.getConfigValue('mq_port')
        self.consumer_exchange = config.getConfigValue('mq_receiver_exchange')
        self.consumer_queue = config.getConfigValue('mq_receiver_queue')
        self.consumer_routing_key = config.getConfigValue('mq_consumer_routing_key')

        credentials = pika.PlainCredentials(self.mq_user,self.mq_pass)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(self.mq_host,  int(self.mq_port), '/', credentials))

        self.channel = self.connection.channel()

        self.channel.queue_declare(queue = self.consumer_queue, arguments = { 'x-message-ttl': 25000 })
        self.channel.queue_bind(exchange = self.consumer_exchange, queue = self.consumer_queue, routing_key = self.consumer_routing_key)
        self.on_message = partial(self.callback)
        self.channel.basic_consume(self.on_message, queue = self.consumer_queue, no_ack=True)


        self.run()

    def run(self):
        self.channel.start_consuming()


    def callback(self, channel ,method, properties, body):
        self.processor.process(body)
        self.connection.close()



