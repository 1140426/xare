import abc


class BrainChannel(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def getReceiverBroker(self, processorv):
        pass

    @abc.abstractmethod
    def getSenderBroker(self, processor):
        pass
