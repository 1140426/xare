import abc


class Sender(object):
    __metaclass__ = abc.ABCMeta


    @abc.abstractmethod
    def send(self, jsonout):
        pass
