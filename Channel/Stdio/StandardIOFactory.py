from StandardIOReceiver import StandardIOReceiver
from StandardIOSender import StandardIOSender


class StandardIOFactory(object):
    def getReceiverBroker(self, config, processor):
        return StandardIOReceiver(processor=processor)

    def getSenderBroker(self, config):
        return StandardIOSender()