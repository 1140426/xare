import sys

from Channel.Receiver import Receiver


class StandardIOReceiver(Receiver):

    def __init__(self, processor):
        self.processor = processor

    def run(self):
        for line in sys.stdin:
            self.processor.process(line)
