/**
* ShelfAi RabbitMQ
*/
(function(){
    var rabbitService =  angular.module('debugger.services.rabbitmq', []);

    rabbitService.service('RabbitMQClient', ['$rootScope', function($rootScope)
    {
        /**
         * @param user user
         * @param pass pass
         * @param queue queue
         * @param host host
         * @param port port
         * @param event event to broadcast when it receives a message
         * @constructor
         */
        function RabbitMQClient(user, pass, queue, host, port, event)
        {
            this.config = {};
            this.config.user = user;
            this.config.pass = pass;
            this.config.queue = queue;
            this.config.host = host;
            this.config.port = port;
            this.config.server = "http://" +  host + ":" + port + "/stomp";
            this.config.event = event;
            
            console.log("[RABBIT] Client created: " + this.config.user + ", " + this.config.pass + ", and queue: " + this.config.queue);
            console.log("[RABBIT] Host: " + this.config.server);
        }


        /**
         * Makes the connection to rabbit after successfully asked for permission
         */
        RabbitMQClient.prototype.connect = function()
        {
            var that = this;
            var ws = new SockJS(this.config.server);
            client = Stomp.over(ws);
            client.heartbeat.outgoing = 0;
            client.heartbeat.incoming = 0;
            client.connect(this.config.user, this.config.pass, function(data) {
                // subscribe to the given queue
                client.subscribe("/amq/queue/"+that.config.queue, function(data)
                {
                    console.log("[RABBIT] message: " + data.body);
                    var message = data.body;
                    $rootScope.$broadcast(that.config.event, {message : message});
                });

                console.log("[RABBIT] Connected.");
            }, this.onError, '/');
        };


		RabbitMQClient.prototype.send = function(message, exchange, routingKey)
		{
			var routingKeys = [ "debug" ];
			routingKeys.push(routingKey);
			
			routingKeys.forEach( function (routingKey)
		  	{
				
				var destination = "/exchange/" + exchange + "/" + routingKey;
				
				client.send(destination, {"content-type":"text/plain"}, message);
                
			});
		}
        /**
         * On connect error callback.
         */
        RabbitMQClient.prototype.onError = function() {
            console.log("[RABBIT] Failed to connect.");
        };

        /**
         *
         * @param user user
         * @param pass pass
         * @param queue queue
         * @param host host
         * @param port port
         * @param event event
         * @returns {RabbitMQClient}
         */
        this.$get = function(user, pass, queue, host, port, event)
        {
            return new RabbitMQClient(user, pass, queue, host, port, event);
        };
    }])
})();

