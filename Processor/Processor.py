import abc


class Processor(object):
    __metaclass__ = abc.ABCMeta
    processingModuleInterface = None

    def __init__(self, processingModuleInterface):
        self.processingModuleInterface = processingModuleInterface

    @abc.abstractmethod
    def process(self, jsonin):
        pass
