import Image
import json
import os
import properties

class CropImage(object):
    def crop_image_by_json_file(self, json_file):
        json_data = json.loads(json_file)

        upper_point_x = json_data[properties.DICT_BOUNDING_BOX_TLC_X]
        upper_point_y = json_data[properties.DICT_BOUNDING_BOX_TLC_Y]

        lower_point_x = json_data[properties.DICT_BOUNDING_BOX_BRC_X]
        lower_point_y = json_data[properties.DICT_BOUNDING_BOX_BRC_Y]

        image_path = json_data[properties.IMAGE_PATH]

        im = Image.open(image_path)

        dir, aux = os.path.split(image_path)

        fn, fext = os.path.splitext(aux)

        # if not os.path.exists(properties.SAVE_FOLDER + fn):
        #     os.makedirs(properties.SAVE_FOLDER + fn)

        im.crop((int(upper_point_x), int(upper_point_y), int(lower_point_x), int(lower_point_y))).save(
            dir+"/" + "crop{}x{}_{}x{}{}".format(upper_point_x, upper_point_y,
                                                                              lower_point_x, lower_point_y, fext))

        pass

# EXEMPLO DE EXECUCAO
#
# crop = CropImage()
# crop.crop_image_by_json_file(
# '{"bounding_box_tlc_x" :"988", "bounding_box_tlc_y":"520","bounding_box_brc_x":"1361", "bounding_box_brc_y":"722","image":"../../TestingImages/etiqueta.png"}')
