import properties

class BoundingBoxCreator(object):
    barCodeDictionary = {}
    barcodeHeightPercentage = None
    barcodeWidthPercentage = None
    barcodeLimit = None
    barcodeWidth = None
    barcodeHeight = None
    boundingBoxDictionary = {}

    def __init__(self, barcodeDictionary, barcodeHeightPercentage, barcodeWidthPercentage, barcodeLimit):
        self.barCodeDictionary = barcodeDictionary
        self.barcodeHeightPercentage = barcodeHeightPercentage
        self.barcodeWidthPercentage = barcodeWidthPercentage
        self.barcodeLimit = barcodeLimit

    #calls all the necessary methods to calculate the bounding box
    def calculateBoundingBox(self):
        self.__calculateBarcodeHeight()
        self.__calculateBarcodeWidth()

        self.__calculateBoundingBoxTopLeftCornerX()
        self.__calculateBoundingBoxTopLeftCornerY()
        self.__calculateBoundingBoxHeight()
        self.__calculateBoundingBoxWidth()
        self.__calculateBoundingBoxBottomRightCornerX()
        self.__calculateBoundingBoxBottomRightCornerY()

    #Returns a textual description of the bounding box (specially useful for .uzn file)
    def generateBoundingBoxJSON(self):
        return json.dumps(boundingBoxDictionary)

    #Returns the barcode height
    def __calculateBarcodeHeight(self):
        self.barcodeHeight = float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_Y]) - float(self.barCodeDictionary[properties.DICT_BARCODE_TOP_LEFT_Y])

    #Returns the barcode width
    def __calculateBarcodeWidth(self):
        self.barcodeWidth = float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_X]) - float(self.barCodeDictionary[properties.DICT_BARCODE_TOP_LEFT_X])

    #Returns the X coordinate of the bounding box top left corner
    def __calculateBoundingBoxTopLeftCornerX(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_X] = (float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_X]) - (float(self.barcodeWidthPercentage) * float(self.barcodeWidth)))

    #Returns the Y coordinate of the bounding box top left corner
    def __calculateBoundingBoxTopLeftCornerY(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_Y] = (float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_Y]) - (float(self.barcodeHeightPercentage) * float(self.barcodeHeight)))

    #Returns the X coordinate of the bounding box bottom right corner
    def __calculateBoundingBoxBottomRightCornerX(self):
         self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_BRC_X] = (float(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_X] + self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_WIDTH]))

    #Returns the Y coordinate of the bounding box bottom right corner
    def __calculateBoundingBoxBottomRightCornerY(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_BRC_Y] = (float(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_Y] + self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_HEIGHT]))

    #Returns the bounding box height
    def __calculateBoundingBoxHeight(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_HEIGHT] = (float(self.barcodeHeight) * (float(self.barcodeHeightPercentage) - float(properties.HEIGHT_LIMIT_FROM_BOTTOM)))

    #Returns the bounding box width
    def __calculateBoundingBoxWidth(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_WIDTH] = (float(self.barcodeLimit) * float(self.barcodeWidth))