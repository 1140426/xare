import properties

class GoogleAPIAnalysis(object):

    cropImage = None
    boundingBoxCreator = None
    barcodeDictionary = None

    def __init__(self, jsonIn):
        self.barcodeDictionary = jsonIn

    def __cropEuros(self, boundingBoxEurosDictionary):
        boundingBoxCreator = BoundingBoxCreator(self.barcodeDictionary,
                                                properties.SIMPLE_EUROS_BB_Y_PERCENTAGE_BARCODE,
                                                properties.SIMPLE_EUROS_BB_X_PERCENTAGE_BARCODE,
                                                properties.SIMPLE_EUROS_LIMIT_PERCENTAGE_BARCODE)

        boundingBoxCreator.calculateBoundingBox()
        boundingBoxJson = boundingBoxCreator.generateBoundingBoxJSON()
        cropImage = CropImage()
        #TODO: Images names are not consistent
        cropImage.crop_image_by_json_file(boundingBoxJson)

    def __cropCents(self, boundingBoxCentsDictionary):
        boundingBoxCreator = BoundingBoxCreator(self.barcodeDictionary,
                                                properties.SIMPLE_CENTS_BB_Y_PERCENTAGE_BARCODE,
                                                properties.SIMPLE_CENTS_BB_X_PERCENTAGE_BARCODE,
                                                properties.SIMPLE_CENTS_LIMIT_PERCENTAGE_BARCODE)

        boundingBoxCreator.calculateBoundingBox()
        boundingBoxJson = boundingBoxCreator.generateBoundingBoxJSON()
        cropImage = CropImage()
        #TODO: Images names are not consistent
        cropImage.crop_image_by_json_file(boundingBoxJson)

    def analyze(self, imagePath):
        #needs to call _cropEuros, and get the image already cropped so it can
        #send to google, shapes the result
        #Also, do the same to cents

        __cropEuros
        #TODO: call google api to analyze euros image and get the result

        __cropCents
        #TODO: call google api to analyze cents image and get the result

        #TODO: Concatenate the results in a single json: jsonOut
        return jsonOut

