#!/bin/bash

#messages
startScreenMessage="Let's train Tesseract in order to recognize and optimize a new language!"
argumentsMessage="**Remember that you must insert this arguments:**"
argumentsDescription="\t#1 - Training Text file path \n\t#2 - Language code (3 letters-code to match ISO 639-2 codes)\n\t#3 - Font name\n"
tutorialMessage="\n\nThis script is based on the official tutorial:"
tutorialURL="https://github.com/tesseract-ocr/tesseract/wiki/TrainingTesseract#introduction"
errorInsufArguments="Please insert the three arguments explained above!!"
breakLine="-------------------------------------------------------------------------------------------\n\n"

echo -e "\n${breakLine}"
echo -e "${startScreenMessage} \n"
echo -e "${tutorialMessage}"
echo -e "${tutorialURL} \n\n"
echo -e "${argumentsMessage}"
echo -e "${argumentsDescription}"
echo -e "${breakLine}"

if [ $# -ne 3 ]; then
	echo "${errorInsufArguments}"
else

	#save arguments into variables
	trainingTextFileName=$1
	languageCode=$2	
	fontName=$3	
	fontNameWithoutBlanks="$(echo -e "${fontName}" | tr -d ' ')"

	
	#change training text file permissions
	chmod 777 $trainingTextFileName


	#generating training and box files
	text2image --text="${trainingTextFileName}" --outputbase="${languageCode}"."${fontNameWithoutBlanks}".exp0 --font="$fontName" --fonts_dir=/usr/share/fonts

#	text2image --text="${trainingTextFileName}" --outputbase="${languageCode}"."${fontNameWithoutBlanks}".exp0 --font='DejaVu Sans Bold' --fonts_dir=/usr/share/fonts

	#run tesseract for training	
	tesseract "${languageCode}"."${fontNameWithoutBlanks}".exp0.tif "${languageCode}"."${fontNameWithoutBlanks}".exp0 box.train

	#generate unicharset file
	#.1	
	unicharset_extractor "${languageCode}"."${fontNameWithoutBlanks}".exp0.box 

	#.2
	set_unicharset_properties -U input_unicharset -O output_unicharset --script_dir=langdata
	
	#create font_properties file regarding font settings
	`echo "DejaVu_Sans_Mono_Bold 0 1 1 0 0" > font_properties`

	#clustering - prototypes creation wiht mftraining and cntraining
	#.1 - mftraining
	mftraining -F font_properties -U unicharset -O "${languageCode}".unicharset "${languageCode}"."${fontNameWithoutBlanks}".exp0.tr

	#.2 - cntraining
	cntraining "${languageCode}"."${fontNameWithoutBlanks}".exp0.tr 

	#put everything together
	#.1 rename files in order to add the language code as a prefix
	echo "renaming file..."
	mv shapetable "${languageCode}".shapetable
	mv normproto "${languageCode}".normproto
	mv inttemp "${languageCode}".inttemp
	mv pffmtable "${languageCode}".pffmtable
	mv unicharset "${languageCode}".unicharset

	#.2 - combine data files
	echo "combining data"
	combine_tessdata "${languageCode}".
	
	#move traineddata file into the correct tesseract directory
	echo "moving traineddata file into /usr/share/tesseract-ocr/tessdata"
	sudo mv "${languageCode}".traineddata /usr/local/share/tessdata/	
	
	echo -e "\n\n**NB:** all configured successfully, you can now use your language running\"tesseract image.jpg output -l "${languageCode}" \""

fi
