import json

import properties


class JSONfileReader(object):
    # json file content
    __fileContent = None

    # initializer
    def __init__(self, jsonString):
        self.__readFromJSONString(jsonString)

    # reads the x position of the top left corner sent in the json file
    def readTopLeftCornerXposition(self):
        return self.__readKeyFromJSONcontent(properties.JSON_TOP_LEFT_COORDINATES)[0]

    # reads the y position of the top left corner sent in the json file
    def readTopLeftCornerYposition(self):
        return self.__readKeyFromJSONcontent(properties.JSON_TOP_LEFT_COORDINATES)[1]

    # reads the x position of the bottom right corner sent in the json file
    def readBottomRightCornerXposition(self):
        return self.__readKeyFromJSONcontent(properties.JSON_BOTTOM_RIGHT_COORDINATES)[0]

    # reads the y position of the bottom right corner sent in the json file
    def readBottomRightCornerYposition(self):
        return self.__readKeyFromJSONcontent(properties.JSON_BOTTOM_RIGHT_COORDINATES)[1]

    def readImagePath(self):
        return self.__readKeyFromJSONcontent(properties.JSON_BARCODE_IMAGE)

    # generic reader from json content
    def __readKeyFromJSONcontent(self, key):
        return self.__fileContent[key]

    # reads the sent as argument json dictionary
    def __readFromJSONString(self, jsonString):
        self.__fileContent = json.loads(jsonString)
