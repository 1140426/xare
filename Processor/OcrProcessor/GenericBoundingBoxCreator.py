import properties

class GenericBoundingBoxCreator(object):
    description = None
    barCodeDictionary = {}
    barcodeHeightPercentage = None
    barcodeWidthPercentage = None
    barcodeLimit = None
    barcodeWidth = None
    barcodeHeight = None
    boundingBoxDictionary = {}

    def __init__(self, barcodeDictionary, barcodeHeightPercentage, barcodeWidthPercentage, barcodeLimit):
        self.description = "description"
        self.barCodeDictionary = barcodeDictionary
        self.barcodeHeightPercentage = barcodeHeightPercentage
        self.barcodeWidthPercentage = barcodeWidthPercentage
        self.barcodeLimit = barcodeLimit

    #calls all the necessary methods to calculate the bounding box
    def calculateBoundingBox(self):
        self.__calculateBarcodeHeight()
        self.__calculateBarcodeWidth()

        self.__calculateBoundingBoxTopLeftCornerX()
        self.__calculateBoundingBoxTopLeftCornerY()
        self.__calculateBoundingBoxHeight()
        self.__calculateBoundingBoxWidth()

    #Returns a textual description of the bounding box (specially useful for .uzn file)
    def generateBoundingBoxDescription(self):
        return (str(int(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_X])) +
               ' ' + str(int(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_Y])) +
               ' ' + str(int(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_WIDTH])) +
               ' ' + str(int(self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_HEIGHT])) +
               ' ' + str(self.description))

    #Returns the barcode height
    def __calculateBarcodeHeight(self):
        self.barcodeHeight = float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_Y]) - float(self.barCodeDictionary[properties.DICT_BARCODE_TOP_LEFT_Y])

    #Returns the barcode width
    def __calculateBarcodeWidth(self):
        self.barcodeWidth = float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_X]) - float(self.barCodeDictionary[properties.DICT_BARCODE_TOP_LEFT_X])

    #Returns the X coordinate of the bounding box top left corner
    def __calculateBoundingBoxTopLeftCornerX(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_X] = (float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_X]) - (float(self.barcodeWidthPercentage) * float(self.barcodeWidth)))

    #Returns the Y coordinate of the bounding box top left corner
    def __calculateBoundingBoxTopLeftCornerY(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_TLC_Y] = (float(self.barCodeDictionary[properties.DICT_BARCODE_BOTTOM_RIGHT_Y]) - (float(self.barcodeHeightPercentage) * float(self.barcodeHeight)))

    #Returns the bounding box height
    def __calculateBoundingBoxHeight(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_HEIGHT] = (float(self.barcodeHeight) * (float(self.barcodeHeightPercentage) - float(properties.HEIGHT_LIMIT_FROM_BOTTOM)))

    #Returns the bounding box width
    def __calculateBoundingBoxWidth(self):
        self.boundingBoxDictionary[properties.DICT_BOUNDING_BOX_WIDTH] = (float(self.barcodeLimit) * float(self.barcodeWidth))