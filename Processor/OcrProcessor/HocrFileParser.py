from HTMLParser import HTMLParser


# subclass of HTMLParser that overrides the handler methods
class HocrFileParser(HTMLParser):
    #counts the lines read by Tesseract
    lineCounter = 0

    #counts chars separated by spaces in each line (note that spaces should not exist, so more than 1 is a bad sign)
    charsPerLineCounter = 0

    #dictionary containing read values and confidence
    readDictionary = {}

    #auxiliar list containing the read chars (used with confidenceList)
    charsList = []

    #auxiliar list containing the confidence for the read chars (used with charsList)
    confidenceList = []

    #flag that controls the reading of a word
    flagReadWord = 0

    #flag that controls the reading of a value
    flagReadValue = 0

    #properties inside the xhtml file
    SPAN_TAG = 'span'
    OCR_LINE_ATTRIBUTE = 'ocr_line'
    OCRX_WORD_ATTRIBUTE = 'ocrx_word'
    TITLE_ATTRIBUTE = 'title'
    CONFIDENCE_ATTRIBUTE = 'x_wconf'
    DIR_ATTRIBUTE = 'dir'

    #called everytime a start tag appears
    def handle_starttag(self, tag, attrs):
        if (tag == "span"):
            # print "****************************tag span************************"
            self.__foundSpanTag(attrs)
            #print ""

    #called everytime data inside tags appears
    def handle_data(self, data):  # tag content
        if (self.flagReadValue == 1):
            self.charsList.append(data)
            self.flagReadValue = 0
            self.__addReadLineToDictionary()
            #print data

    #called in order to process a tag called span (where tesseract places info about each line read)
    def __foundSpanTag(self, attributes):
        for attr in attributes:
            if (attr[1] == self.OCR_LINE_ATTRIBUTE):  # one line can have multiple reads, separated by spaces!!
                #print ""
                #print "********************* new line **********************"
                self.__prepare4NewLine()

            if (attr[1] == self.OCRX_WORD_ATTRIBUTE):
                self.flagReadWord = 1  # will read a word!

            self.__processWord(attr)

            #print "-> atributte:  ", attr

    #adds a read line info to the dictionary , containing words read and confidence per word
    def __addReadLineToDictionary(self):
        auxList = []
        auxList.append(self.charsList)
        auxList.append(self.confidenceList)

        self.readDictionary[str(
            self.lineCounter)] = auxList  # added to the dictionary a list with the line nuber and read results + confidence

    #prepares a new line to be read by incrementing line counter and cleaning chars and confidence list
    def __prepare4NewLine(self):
        self.lineCounter += 1  # new line
        # self.charsPerLineCounter = 0
        self.charsList = []
        self.confidenceList = []

    #processes a word read and appends info to the dictionaries. controls, also, when a new price will be read within a tag
    def __processWord(self, attr):
        if (self.flagReadWord == 1):  # we are reading a succession of attributes regarding a word
            if (attr[0] == self.TITLE_ATTRIBUTE):  # process confidence here!
                auxTitle = attr[1]
                delimited = auxTitle.split(';')
                for word in delimited:
                    if (self.CONFIDENCE_ATTRIBUTE in word):
                        word = word.strip()
                        auxConfidence = word.split(' ')
                        self.confidenceList.append(auxConfidence[1])

            if (attr[0] == self.DIR_ATTRIBUTE):  # end of the word
                self.flagReadValue = 1  # informs that a value needs to be read and added to the list
                self.flagReadWord = 0
