import json
import os
import subprocess

import properties
from GenericBoundingBoxCreator import GenericBoundingBoxCreator
from HocrFileAnalyzer import  HocrConfidenceAnalyzer
from JSONReader import JSONfileReader


class AnalysisStrategy(object):
    # python doc
    analysisType = None

    jsonContent = None

    def __init__(self, analysisType, jsonContent):
        self.analysisType = analysisType
        self.jsonContent = jsonContent

    #Method responsible for creating the bounding box,  generate a textual description of it and process the image
    def analyze(self):
        jsonContent = self.__readJSON()
        readResult = []

        if (self.analysisType == properties.ANALYSIS_SIMPLE_TYPE):
            # Euros processing
            eurosBoundingBoxCreator = GenericBoundingBoxCreator(jsonContent,
                                                                properties.SIMPLE_EUROS_BB_Y_PERCENTAGE_BARCODE,
                                                                properties.SIMPLE_EUROS_BB_X_PERCENTAGE_BARCODE,
                                                                properties.SIMPLE_EUROS_LIMIT_PERCENTAGE_BARCODE)
            eurosBoundingBoxCreator.calculateBoundingBox()
            prettyBoundingBoxEuros = eurosBoundingBoxCreator.generateBoundingBoxDescription()

            #only for debug purpose
            print(prettyBoundingBoxEuros)

            # tesseract read
            priceConfidenceEuros = self.__processImage(jsonContent[properties.DICT_BARCODE_IMAGE],
                                                          prettyBoundingBoxEuros)

            #-----------------------------------------------------------------------------------------------------------
            #Cents processing
            centsBoundingBoxCreator = GenericBoundingBoxCreator(jsonContent,
                                                                properties.SIMPLE_CENTS_BB_Y_PERCENTAGE_BARCODE,
                                                                properties.SIMPLE_CENTS_BB_X_PERCENTAGE_BARCODE,
                                                                properties.SIMPLE_CENTS_LIMIT_PERCENTAGE_BARCODE)
            centsBoundingBoxCreator.calculateBoundingBox()
            prettyBoudingBoxCents = centsBoundingBoxCreator.generateBoundingBoxDescription()

            #only for debug purpose
            print(prettyBoudingBoxCents)

            # tesseract read
            priceConfidenceCents = self.__processImage(jsonContent[properties.DICT_BARCODE_IMAGE],
                                                          prettyBoudingBoxCents)

            #get read result and confidence (average)
            readPrice = priceConfidenceEuros[0] + "," + priceConfidenceCents[0]
            confidence = int((int(priceConfidenceEuros[1])+int(priceConfidenceCents[1]))/2)
            readResult.append(readPrice)
            readResult.append(confidence)


        else:
            if (self.analysisType == properties.ANALYSIS_COMPLEX_TYPE):
                complexBoundingBoxCreator = GenericBoundingBoxCreator(jsonContent,
                                                                      properties.COMPLEX_BB_Y_PERCENTAGE_BARCODE,
                                                                      properties.COMPLEX_BB_X_PERCENTAGE_BARCODE,
                                                                      properties.COMPLEX_LIMIT_PERCENTAGE_BARCODE)
                complexBoundingBoxCreator.calculateBoundingBox()
                prettyBoudingBoxComplex = complexBoundingBoxCreator.generateBoundingBoxDescription()

                #only for debug purpose
                print(prettyBoudingBoxComplex)

                #read tesseract, process confidence and save into read result
                readResult = self.__processImage(jsonContent[properties.DICT_BARCODE_IMAGE], prettyBoudingBoxComplex)

        readResult[0] = self.__removeNonAsciiChar(readResult[0])
        readResult[0] = self.__cleanPriceValue(readResult[0])
        readResult = self.__createJsonWithResult(readResult)
        return readResult

    #Reads a JSON containing information about the image
    def __readJSON(self):
        jsonParser = JSONfileReader(self.jsonContent)

        readContent = {properties.DICT_BARCODE_TOP_LEFT_X: jsonParser.readTopLeftCornerXposition(),
                       properties.DICT_BARCODE_TOP_LEFT_Y: jsonParser.readTopLeftCornerYposition(),
                       properties.DICT_BARCODE_BOTTOM_RIGHT_X: jsonParser.readBottomRightCornerXposition(),
                       properties.DICT_BARCODE_BOTTOM_RIGHT_Y: jsonParser.readBottomRightCornerYposition(),
                       properties.DICT_BARCODE_IMAGE: jsonParser.readImagePath()}

        return readContent

    #Creates a .uzn file with the content received by argument
    def __saveIntoUZN(self, filepath, uznContent):
        (directory, imageName) = os.path.split(filepath)
        uznFileName = imageName.split(".")[0] + ".uzn"

        if (directory != ""):
            uznFileName = directory + "/" + uznFileName

        f = open(uznFileName, 'w')
        f.write(uznContent)
        f.close()

    #Reads the result of the image processing by Tesseract, saved into the XHTML file
    def __readResultFromHocr(self, imagePath):
        (directory, imageName) = os.path.split(imagePath)
        fileName = imageName.split(".")[0] + '.hocr'

        parser = HocrConfidenceAnalyzer(fileName)
        readResults = parser.processConfidencePerReadResult()

        higherResult = parser.resultWithHigherConfidence()
        print higherResult

        return higherResult

    #Removes non Ascii characters of the string received by parameter
    def __removeNonAsciiChar(self, str):
        return "".join(i for i in str if ord(i) < 128)

    #Removes duplicated commas and whitespaces
    def __cleanPriceValue(self, price):
        cleanPrice = []

        for c in price:
            if (c == ","):
                if (c not in cleanPrice): #only one comma per price
                    cleanPrice.append(c)
            else:
                if(c != " "): #if not a white space
                    cleanPrice.append(c) #not a comma

        return ''.join(cleanPrice)


    #Creates a JSON file with the result of the Tesseract processing received by argument
    def __createJsonWithResult(self, readResult):
        data = {}
        data[properties.JSON_TESSERACT_RESULT] = readResult[0]
        data[properties.JSON_TESSERACT_CONFIDENCE] = readResult[1]
        return json.dumps(data)

    #Saves the bounding box information into a UZN file and calls tesseract to analyze the image
    def __processImage(self, imagePath, uznFileContent):
        # creates UZN file
        self.__saveIntoUZN(imagePath, uznFileContent)

        # call tesseract script and creates a file with the result (fileName.txt) in the directory from which it was called
        subprocess.call([properties.TESSERACT_SCRIPT_NAME, imagePath])

        # goes to fileName.txt, created by Tesseract, gets the result and takes away '\n'
        priceAndConfidence =  self.__readResultFromHocr(imagePath)

        """
        price = priceAndConfidence[0] # 0 for price!!
        confidence = priceAndConfidence[1] # 1 for confidence!!

                print "****"
                print "price = ", price
                print "confidence = ", confidence
                print "****"
        """

        #delete uzn and txt files
        subprocess.call([properties.DELETE_UZN_HOCR_FILES, imagePath])
        return priceAndConfidence
