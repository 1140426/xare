import glob
import unittest

""" The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell,
    although results are returned in arbitrary order."""

testFiles = glob.glob('*Test.py')

#creates a module with all the test files' names and a module of test cases
moduleStrings = [testFile[0:len(testFile)-3] for testFile in testFiles]
testCases = [unittest.defaultTestLoader.loadTestsFromName(test_file) for test_file in moduleStrings]

#creates a test suite containing all the tests in the *_test.py files
testSuite = unittest.TestSuite(testCases)
testRunner = unittest.TextTestRunner().run(testSuite)