from Processor.Processor import Processor


class TestProcessor(Processor):
    def __init__(self, processingModuleInterface):
        super(TestProcessor, self).__init__(processingModuleInterface)
        #Processor.__init__(self,processingModuleInterface)

    def process(self, json):
        '''
        Process something and send the output
        '''
        number = int(json) * int(json)
        self.processingModuleInterface.send(json, str(number))
