import sys

sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule


class ZbarProcessingModule(ProcessingModule):
    def __init__(self):
        super(ZbarProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(ZbarProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(ZbarProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(ZbarProcessingModule, self).setMemory(key, value)


module = ZbarProcessingModule()