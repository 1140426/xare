import sys
sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule

class CropProcessingModule(ProcessingModule):

    def __init__(self):
        super(CropProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(CropProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(CropProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(CropProcessingModule, self).setMemory(key, value)


module = CropProcessingModule()