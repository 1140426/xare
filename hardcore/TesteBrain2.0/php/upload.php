<?php

require __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;



header('Access-Control-Allow-Origin: *');

$year=date('Y');
$month=date('m');
$day=date('d'); 


$oldmask = umask(0);
mkdir('uploads/'.$year.'/'.$month.'/'.$day, 0777, true);
umask($oldmask);

$targetPath = "uploads"."/".$year."/".$month."/".$day."/";

if(isset($_FILES['uploadedfile'])){

if(!isset($_POST['deviceId'])){
  $deviceID = "No deviceId";
}else{
  $deviceID= $_POST['deviceId'];
}

 $fileName = $_FILES['uploadedfile']['name'];

$fileNameVec=explode('.',$fileName);
 $fileType=$fileNameVec[1];
 date_default_timezone_set('Europe/Lisbon');
 $date = date('m/d/Yh:i:sa', time());
 $rand=rand(10000,99999);
 $encname=$date.$rand;
 $fileName=md5($encname).'.'.$fileType;
 $filePath=$fileName;
 
 $targetPath = $targetPath.$filePath; 

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $targetPath)) {
    echo "The file ".  basename( $_FILES['uploadedfile']['name']). 
    " has been uploaded";
    
   $image_path = str_replace('upload.php', $targetPath, $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); 

    $config = json_decode(file_get_contents("config.json"), true);

    $rabbitmq_config = $config['rabbitmq'];

    $host = $rabbitmq_config['connection']['URL'];
    $port = $rabbitmq_config['connection']['port'];
    $user = $rabbitmq_config['connection']['user'];
    $password = $rabbitmq_config['connection']['password'];
    $vhost = $rabbitmq_config['connection']['vhost'];
    $rabbitmq_config_send = $rabbitmq_config['send'];
    $send_exchange_name = $rabbitmq_config_send['exchange']['name'];

    $send_exchange_type = $rabbitmq_config_send['exchange']['type'];
    $connection = new AMQPStreamConnection($host, $port, $user, $password, $vhost);
$channel = $connection->channel();
$channel->exchange_declare($send_exchange_name,$send_exchange_type, false, false, false);
$data = implode(' ', array_slice($argv, 1));

$arr = array('filePath' => $image_path, 'deviceID' => $deviceID);
    
    if(empty($data)) $data = json_encode($arr);
    
$msg = new AMQPMessage($data);
$channel->basic_publish($msg, $send_exchange_name);
$channel->close();
$connection->close();
} else{
    echo "There was an error uploading the file, please try again!";
}
}else{
  echo "Theres was no file to upload";
}


?>
