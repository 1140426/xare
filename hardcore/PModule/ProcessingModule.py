import abc
import sys
import importlib
import argparse
from os.path import dirname
import os

sys.path.append("../")
from Channel.BrainChannelFactory import BrainChannelFactory
from Config import Config


class ProcessingModule(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):

        parser = argparse.ArgumentParser(description="Run " + self.__class__.__name__)
        parser.add_argument("configfile", help="Config file for " + self.__class__.__name__)
        parser.add_argument("-r", "--receiver", help="Brain Channel Receiver channel (default: standardio)", choices=["standardio","rabbitmq"], default="standardio")
        parser.add_argument("-s", "--sender", help="Brain Channel Sender channel (default: standardio)", choices=["standardio","rabbitmq"], default="standardio")
        parser.add_argument("-d", "--debug", help="Enable debug mode via rabbitmq", action="store_true")
        parameters = parser.parse_args()

        self.config = Config(parameters.configfile)
        self.receivers = []
        processorClass = self.import_class(self.config.getConfigValue("processor"))
        self.processor = processorClass(self)
        self.threads = []
        self.rchannel = parameters.receiver
        self.schannel = parameters.sender
        self.debug_config = None

        if(parameters.debug):
            self.debug_config = Config(self.config.getConfigValue('debug_config_file'))
            print "Open Brain Debugger with browser:"
            print "file://"+dirname(os.path.dirname(os.path.abspath(__file__)))+\
                  "/braindebugger/app/index.html?user="+self.debug_config.getConfigValue('mq_user')+\
                  "&pass=" + self.debug_config.getConfigValue('mq_pass')+\
                  "&host=" + self.debug_config.getConfigValue('mq_host')+ \
                  "&port=5001#/debug"
            self.bcSender = BrainChannelFactory.get_factory("rabbitmq").getSenderBroker(self.debug_config)
        else:
            self.bcSender = BrainChannelFactory.get_factory(self.schannel).getSenderBroker(self.config)

        if(parameters.debug):
            self.createReceiver("rabbitmq",self.debug_config)
        else:
            for i in range(0, int(self.config.getConfigValue("consumers"))):
                self.createReceiver(self.rchannel,self.config)


    def createReceiver(self,rchannel,config):
        receiver = BrainChannelFactory.get_factory(rchannel).getReceiverBroker(config, self.processor)
        self.receivers.append(receiver)
        receiver.run()

    def import_class(self, cl):
        d = cl.rfind(".")
        classname = cl[d + 1:len(cl)]
        package = cl[0:d]
        class_ = getattr(importlib.import_module(package), classname)
        return class_


    def getConfig(self, key):
        return self.config.getConfigValue(key)

    @abc.abstractmethod
    def send(self, jsonin, jsonout):
        self.bcSender.send(jsonout)

    @abc.abstractmethod
    def getMemory(self,key):
        pass

    @abc.abstractmethod
    def setMemory(self,key,value):
        pass
