import sys
sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule

class OcrProcessingModule(ProcessingModule):

    def __init__(self):
        super(OcrProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(OcrProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(OcrProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(OcrProcessingModule, self).setMemory(key, value)


module = OcrProcessingModule()
