import sys

sys.path.append("../../")
from PModule.ProcessingModule import ProcessingModule


class ZxingProcessingModule(ProcessingModule):
    def __init__(self, args):
        super(ZxingProcessingModule, self).__init__(args)

    def send(self, jsonin, jsonout):
        super(ZxingProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(ZxingProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(ZxingProcessingModule, self).setMemory(key, value)


module = ZxingProcessingModule(sys.argv[1:])
