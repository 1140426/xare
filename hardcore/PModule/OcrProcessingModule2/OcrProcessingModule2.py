import sys

sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule


class OcrProcessingModule2(ProcessingModule):
    def __init__(self):
        super(OcrProcessingModule2, self).__init__()

    def send(self, jsonin, jsonout):
        super(OcrProcessingModule2, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(OcrProcessingModule2, self).getMemory(key)

    def setMemory(self, key, value):
        super(OcrProcessingModule2, self).setMemory(key, value)


module = OcrProcessingModule2()
