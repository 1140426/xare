import sys
sys.path.append("../")
from PModule.ProcessingModule import ProcessingModule

class JudgementProcessingModule(ProcessingModule):

    resultList = []

    def __init__(self):
        super(JudgementProcessingModule, self).__init__()

    def send(self, jsonin, jsonout):
        super(JudgementProcessingModule, self).send(jsonin, jsonout)

    def getMemory(self, key):
        super(JudgementProcessingModule, self).getMemory(key)

    def setMemory(self, key, value):
        super(JudgementProcessingModule, self).setMemory(key, value)


module = JudgementProcessingModule()
