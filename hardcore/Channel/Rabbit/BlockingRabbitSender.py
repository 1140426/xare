import pika

from Channel.Sender import Sender


class BlockingRabbitSender(Sender):


    def __init__(self,config):
        self.mq_user = config.getConfigValue('mq_user')
        self.mq_pass = config.getConfigValue('mq_pass')
        self.mq_host = config.getConfigValue('mq_host')
        self.mq_port = config.getConfigValue('mq_port')
        self.publish_exchange = config.getConfigValue('mq_publish_exchange')
        self.publish_rk = config.getConfigValue('mq_publish_routing_key')

        self.credentials = pika.PlainCredentials(self.mq_user,self.mq_pass)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(self.mq_host,  int(self.mq_port), '/', self.credentials))
        self.channel = self.connection.channel()


    def send(self, jsonout):
        print jsonout
        self.channel.basic_publish(exchange=self.publish_exchange, routing_key=self.publish_rk, body=jsonout)