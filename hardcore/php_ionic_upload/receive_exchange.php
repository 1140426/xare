<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('localhost', 5672, 'piloto', 'piloto', 'piloto');
$channel = $connection->channel();

$channel->exchange_declare('images_exchange', 'fanout', true, false, false);

list($queue_name, ,) = $channel->queue_declare("images", false, true, false, false);

$channel->queue_bind($queue_name, 'images_exchange');

echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";

$callback = function($msg){

$obj = json_decode($msg->body);

  echo ' [x] ', 'FilePath: ', $obj->{'filePath'}, ' X: ', $obj->{'x'}, ' Y: ', $obj->{'y'}, "\n";
};

$channel->basic_consume($queue_name, 'images', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

?>