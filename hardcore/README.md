Fist steps  (Command line instructions)
==========

## Clonning the repossitory

### Git global setup
```bash 
git config --global user.name "<YOUR NAME>" 
git config --global user.email "<YOUR EMAIL>" 
```

### SSH KEY
```bash 
# If you have alredy have an ssh key you can pull the next line
ssh-keygen # Do not answer the questions

cat ~/.ssh/*.pub

# Copy the information and put the ssh key in your profile settings
ssh-add
```

### Clone repository
```bash
git clone ssh://git@gitlab.xareserver.dyndns.org:1026/vasco/Brain2.0.git

cd Brain2.0
```

## Runing DOCKER Containers

### Install Docker
1. Install docker toolbox for your operating system
    * WINDOWS OR MAC [link](https://www.docker.com/products/docker-toolbox)
    * LINUX `wget -qO- https://get.docker.com/ | sh`
2. `docker load < docker/processingModule.tar`
3. `docker run --rm=true -ti -v $(pwd):/src processing_module:latest /bin/bash`
    * ATTENTION: The above command should be running in `project root path`
4. Your application is running

### How to open a closed conteiner?
1. `docker ps -a` to see all the containers
2. `docker start <container id or name>`
3. Your conteiner is running again

### How get inside an docker conteiner alredy started?
1. `docker ps -a` to see all the containers
2. `docker attach  <container id or name>`
3. You are in inside your container

### How to connect with the docker container of RabbitMQ?
1. Config your config file to use `host=172.17.0.1` and `port=5000`
2. run `docker load < docker/rabbitmq4brain.tar`
3. run `docker run -d -ti --name rabbitmq_test -p 8000:15672 -p 5000:5672 rabbitmq4brain:latest` in your terminal
4. RabbitMQ is running in your machine
    * You can use the management board in `http://localhost:8000`

### How to connect with the docker container of elasticSearch an kibana?
1. Config your config file to use `host=172.17.0.1` and `port=5001`
2. 2. run `docker load < docker/elastic_kibana.tar`
3. run `docker run -ti --name elastic_kibana_test -p 8001:5601 -p 5001:9200 elastic_kibana:latest` in your terminal 
4. RabbitMQ is running in your machine
    * You can use the kibana board in `http://localhost:8001`
    * You can use log in to`port=8001`

## File Structure
```bash
├── BrainPModuleController.py
├── PModule
│   ├── ProcessingModule.py
│   ├── TestProcessingModule
│   │   ├── TestProcessingModule.py
│   │   ├── test.cfg
├── Processor
│   ├── Processor.py
│   ├── TestProcessor
│   │   ├── TestProcessor.py
├── docker
│   ├── elastic_kibana.tar
│   ├── rabbitmq4brain.tar
│   ├── processingModule.tar
```

## IN DEV MODE

### New commit
```bash
git add FILENAME
git commit -m "Message"
git push
```

### Add new Processor
Processors should have their own package with all their dependencies (check TestProcessor folder)

### Add new ProcessingModule
Each processor should have a ProcessingModule.py and its own .cfg file which specifies the processor that should be run (check TestProcessingModule.py and test.cfg).
Imports should be reviewed accordingly.

### Running
Processors can be used with ABCProcessingModule.py. Using stdio (default) as receiver and sender (currently supported: standardio, rabbitmq), the following command can be used: 

```
echo "{jsonin}" | python ABCProcessingModule.py config.cfg

 Example to test the output for ZbarProcessor:

 cd PModule

 Rotator:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}' | python RotatorProcessingModule/RotatorProcessingModule.py RotatorProcessingModule/rotator.cfg

 Zbar:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}' | python ZbarProcessingModule/ZbarProcessingModule.py ZbarProcessingModule/zbar.cfg

 Ocr:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}' | OcrProcessingModule/OcrProcessingModule.py OcrProcessingModule/Ocr.cfg

 [Rotator,Zbar]:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}' | python RotatorProcessingModule/RotatorProcessingModule.py RotatorProcessingModule/rotator.cfg | python ZbarProcessingModule/ZbarProcessingModule.py ZbarProcessingModule/zbar.cfg

 [Zbar,OCR]:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}' | python ZbarProcessingModule/ZbarProcessingModule.py ZbarProcessingModule/zbar.cfg | OcrProcessingModule/OcrProcessingModule.py OcrProcessingModule/Ocr.cfg

 [Rotator,Zbar,Ocr]:
 echo '{"image":"/src/TestingImages/image.extension","label_type":"Jumbo A"}'| python RotatorProcessingModule/RotatorProcessingModule.py RotatorProcessingModule/rotator.cfg | python ZbarProcessingModule/ZbarProcessingModule.py ZbarProcessingModule/zbar.cfg | OcrProcessingModule/OcrProcessingModule.py OcrProcessingModule/Ocr.cfg



```

For Usage: 
```
ABCProcessingModule.py -h
```

### Config File (.cfg)
```
[config]
log_location = /var/log/xarevision/
encoding = latin1
consumers = 1
processor = Processor.TestProcessor.TestProcessor.TestProcessor
#rabbitmq settings (optional if using standard in/out)
mq_user = guest
mq_pass = guest
rabbitmq_amqp_url = amqp://guest:guest@localhost:5672/%2F
rabbitmq_consumer_exchange = layer1
rabbitmq_consumer_queue = Processor_queue
rabbitmq_consumer_routing_key = layer1
rabbitmq_publish_routing_key = layer2
rabbitmq_publisher_exchange = layer2
rabbitmq_exchange_type = topic
```