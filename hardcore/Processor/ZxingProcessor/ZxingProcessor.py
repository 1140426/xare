#!/usr/bin/env python
import json
import sys

sys.path.append("../")
from Processor.Processor import Processor
from ZxingWrapper import *


class ZxingProcessor(Processor):
    processingModuleInterface = None
    ZXING_LIBRARY = "./zxing/"
    JSON_IMAGE = "image"
    JSON_LABEL_TYPE = "label_type"
    JSON_BARCODE = "barcode"
    JSON_UPPER_POINT = "upper_point"
    JSON_LOWER_POINT = "lower_point"

    def __init__(self, processingModuleInterface):
        super(ZxingProcessor, self).__init__(processingModuleInterface)

    def process(self, jsonIn):
        """This function receives a json string as parameter and tries decode the barcode
        from the image whose path is inside the json file"""

        # create a json objects with the json string received by parameter
        jsonData = json.loads(jsonIn)

        # find the image inside the json object
        imagePath = str(jsonData[self.JSON_IMAGE])

        # create a barcode reader
        barcodeReader = BarCodeReader(self.ZXING_LIBRARY)

        # finds barcode data from image
        barcode = barcodeReader.decode(imagePath, True)

        # create a json output with the image path and label type decoded from jsonIn
        jsonOut = {self.JSON_IMAGE: imagePath, self.JSON_LABEL_TYPE: str(jsonData[self.JSON_LABEL_TYPE])}

        # if there is some data processed
        if barcode is not None:
            # add the code to jsonOut
            jsonOut[self.JSON_BARCODE] = barcode.data

            # add the top left corner coordinates to jsonOut
            jsonOut[self.JSON_UPPER_POINT] = barcode.points[0][0], barcode.points[0][1]

            # add the bottom right corner coordinates to jsonOut
            jsonOut[self.JSON_LOWER_POINT] = barcode.points[1][0], barcode.points[1][0]

        # send the jsonIn and jsonOut to the next layer
        self.processingModuleInterface.send(jsonIn, str(jsonOut))
