#euro and cents analysis, one at a time
ANALYSIS_SIMPLE_TYPE = 'simple'

#euro + cents together analysis
ANALYSIS_COMPLEX_TYPE = 'complex'

#height limit from the bottom of the barcode till the digits base
HEIGHT_LIMIT_FROM_BOTTOM = 0.0987

#width percentage for euros: dictates the left limit of the read
SIMPLE_EUROS_BB_X_PERCENTAGE_BARCODE = 8.5253

#height percentage for euros: dictates the top limit of the read
SIMPLE_EUROS_BB_Y_PERCENTAGE_BARCODE = 0.6589

#width percentage for euros: dictates the right limit of the read
SIMPLE_EUROS_LIMIT_PERCENTAGE_BARCODE = 5.234

#width percentage for cents: dictates the left limit of the read
SIMPLE_CENTS_BB_X_PERCENTAGE_BARCODE = 3.0316

#height percentage for cents: dictates the top limit of the read
SIMPLE_CENTS_BB_Y_PERCENTAGE_BARCODE = 0.395

#width percentage for cents: dictates the right limit of the read
SIMPLE_CENTS_LIMIT_PERCENTAGE_BARCODE = 1.563

#width percentage for both euros and cents read: dictates the left limit of the read
COMPLEX_BB_X_PERCENTAGE_BARCODE = 8.5253

#height percentage for both euros and cents read: dictates the top limit of the read
COMPLEX_BB_Y_PERCENTAGE_BARCODE = 0.6589

#width percentage for both euros and cents read: dictates the right limit of the read
COMPLEX_LIMIT_PERCENTAGE_BARCODE = 7.044

#barcode top left x position key for intern dictionary use
DICT_BARCODE_TOP_LEFT_X = "tlc_x"

#barcode top left y position key for intern dictionary use
DICT_BARCODE_TOP_LEFT_Y = "tlc_y"

#barcode bottom right x position key for intern dictionary use
DICT_BARCODE_BOTTOM_RIGHT_X = "brc_x"

#barcode bottom right y position key for intern dictionary use
DICT_BARCODE_BOTTOM_RIGHT_Y = "brc_y"

#bounding box top left x position key for intern dictionary use
DICT_BOUNDING_BOX_TLC_X = "bounding_box_tlc_x"

#bounding box top left y position key for intern dictionary use
DICT_BOUNDING_BOX_TLC_Y = "bounding_box_tlc_y"

#bounding box heigt size key for intern dictionary use
DICT_BOUNDING_BOX_HEIGHT = "bounding_box_height"

#bounding box width size key for intern dictionary use
DICT_BOUNDING_BOX_WIDTH = "bounding_box_width"

#image name key for intern dictionary use
DICT_BARCODE_IMAGE = "barcode_image"

#top left coordinates key for json use
JSON_TOP_LEFT_COORDINATES = "upper_point"

#bottom right coordinates key for json use
JSON_BOTTOM_RIGHT_COORDINATES = "lower_point"

#top left coordinates for json use
#JSON_LABEL_TYPE = "label_type"

#image name key for intern json use
JSON_BARCODE_IMAGE = "image"

#tesseract script: responsible for reading the image content when an uzn file containing bounding box measurements exists
TESSERACT_SCRIPT_NAME = "../Processor/OcrProcessor/run_tesseract.sh"

#script that removes both uzn and txt temporary files
DELETE_UZN_HOCR_FILES = "../Processor/OcrProcessor/deleteUZNandHocrFiles.sh"

#result key for the outup json 
JSON_TESSERACT_RESULT = "result"

#confidence processed by tesseract (either total for the compleate reading or average for the separate one)
JSON_TESSERACT_CONFIDENCE = "confidence"
