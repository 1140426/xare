#!/bin/bash

#NB: This Script deletes both .txt and .uzn files. The first one is created by Tesseract and contains the output, the
#    second one contains the zones to be processed by Tesseract.
#


#get image name with no extension, to create the files to remove
imageName=$(basename $1)
dirName=$(dirname $1)
imageNameWithoutExtension=$(echo $imageName | cut -f1 -d.)

uznFile="${dirName}/${imageNameWithoutExtension}.uzn"

#the .txt file is created in the same directory as it is called
hocrFile="${imageNameWithoutExtension}.hocr"

#delete images
rm "${uznFile}" "${hocrFile}"
