#!/bin/sh

#check if system is updated
sudo apt-get update

#install tesseract and both portuguese and english languages
sudo apt-get install tesseract-ocr
sudo apt-get install tesseract-ocr-eng
sudo apt-get install tesseract-ocr-por

#install libraries for tesseract readings
sudo apt-get install libjpeg8 libjpeg62-dev libfreetype6 libfreetype6-dev

#install imagemagik
sudo apt-get install imagemagick

#install pillow (python)
#sudo pip install Pillow

