#!/bin/bash

#NB: This Script assumes that a single argument is sent containing the PATH TO THE IMAGE
#    In order to perform the analysis, it will also assume that a zone file (.uzn) containing the SAME NAME AS THE IMAGE,
#    (except for the extension) will be placed in the same directory as the image!
#
#    The "-psm 4" argument in the command is responsible for telling Tesseract to use the zone file!
#
#    Since the result of the Tesseract analysis is placed inside a file containing the same name as the image (but .txt),
#    that file will exist in the same directory this script is called. You can modify its name by changing the variable
#    called "outputFileNameileName"
#
#get image name with no extension, to create the output file
outputFileName=$(basename $1)
outputFileName=$(echo $outputFileName | cut -f1 -d.)

#echo "${outputFileName}"
#read content
tesseract $1 "${outputFileName}" -psm 4 -l jmb confidence

