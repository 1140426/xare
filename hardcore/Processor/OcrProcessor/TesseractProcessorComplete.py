from AnalysisStrategy import AnalysisStrategy
import properties,sys
sys.path.append("../../")

from Processor.Processor import Processor


class TesseractProcessorComplete(Processor):

    processingModuleInterface = None

    def __init__(self, processingModuleInterface):
        self.processingModuleInterface = processingModuleInterface

        # Next 2 lines are just for testing purpose
        #jsonAux = '{"deviceID":"akjdwofheoihf121we","barcode":"3219110333596","image":"etiqueta.png","label_type":"JumboA","lower_point":[1470,694],"upper_point":[1312,137]}'
        #self.process(jsonAux)

    def process(self, jsonin):
        # complete analysis
        analysisStrategy = AnalysisStrategy(properties.ANALYSIS_COMPLEX_TYPE, jsonin)
        jsonToSend = analysisStrategy.analyze()
        jsonToSend.strip()
        #print(jsonToSend)
        self.processingModuleInterface.send(jsonin, jsonToSend)