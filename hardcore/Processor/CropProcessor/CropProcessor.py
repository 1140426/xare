sys.path.append("../")
from Processor.Processor import Processor

class CropProcessor(Processor):
    processingModuleInterface = None

    def __init__(self, processingModuleInterface):
        self.processingModuleInterface = processingModuleInterface

    def process(self, jsonin):
        #crops the image, sends to google API and stores the result
        googleAnalysis = GoogleAPIAnalysis(jsonin)
        jsonToSend = googleAnalysis.analyze()
        self.processingModuleInterface.send(jsonin, jsonToSend)