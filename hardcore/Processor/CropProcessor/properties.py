#upper point x of the image that will be cropped
CROP_UPPER_POINT_X = "upper_point_x"

#upper point y of the image that will be cropped
CROP_UPPER_POINT_Y = "upper_point_y"

#lower point x of the image that will be cropped
CROP_LOWER_POINT_X = "lower_point_x"

#lower point y of the image that will be cropped
CROP_LOWER_POINT_Y = "lower_point_y"

#image path
IMAGE_PATH = "image"

#folder in which the cropped image will be saved
SAVE_FOLDER = "../../TestingImages/cropImages/"

#height limit from the bottom of the barcode till the digits base
HEIGHT_LIMIT_FROM_BOTTOM = 0.0987

#width percentage for euros: dictates the left limit of the read
SIMPLE_EUROS_BB_X_PERCENTAGE_BARCODE = 8.5253

#height percentage for euros: dictates the top limit of the read
SIMPLE_EUROS_BB_Y_PERCENTAGE_BARCODE = 0.6589

#width percentage for euros: dictates the right limit of the read
SIMPLE_EUROS_LIMIT_PERCENTAGE_BARCODE = 5.234

#width percentage for cents: dictates the left limit of the read
SIMPLE_CENTS_BB_X_PERCENTAGE_BARCODE = 3.0316

#height percentage for cents: dictates the top limit of the read
SIMPLE_CENTS_BB_Y_PERCENTAGE_BARCODE = 0.395

#width percentage for cents: dictates the right limit of the read
SIMPLE_CENTS_LIMIT_PERCENTAGE_BARCODE = 1.563

#barcode top left x position key for intern dictionary use
DICT_BARCODE_TOP_LEFT_X = "tlc_x"

#barcode top left y position key for intern dictionary use
DICT_BARCODE_TOP_LEFT_Y = "tlc_y"

#barcode bottom right x position key for intern dictionary use
DICT_BARCODE_BOTTOM_RIGHT_X = "brc_x"

#barcode bottom right y position key for intern dictionary use
DICT_BARCODE_BOTTOM_RIGHT_Y = "brc_y"

#bounding box top left x position key for intern dictionary use
DICT_BOUNDING_BOX_TLC_X = "bounding_box_tlc_x"

#bounding box top left y position key for intern dictionary use
DICT_BOUNDING_BOX_TLC_Y = "bounding_box_tlc_y"

#bounding box bottom right corner x position key for intern dictionary use
DICT_BOUNDING_BOX_BRC_X = "bounding_box_brc_x"

#bounding box bottom right corner y position key for intern dictionary use
DICT_BOUNDING_BOX_BRC_Y = "bounding_box_brc_y"

#bounding box heigt size key for intern dictionary use
DICT_BOUNDING_BOX_HEIGHT = "bounding_box_height"

#bounding box width size key for intern dictionary use
DICT_BOUNDING_BOX_WIDTH = "bounding_box_width"