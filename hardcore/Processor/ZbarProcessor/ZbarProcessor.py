#!/usr/bin/env python
import json
import sys

sys.path.append("../")

from SimpleCV import Image

from Processor.Processor import Processor


class ZbarProcessor(Processor):
    JSON_IMAGE = "image"
    JSON_LABEL_TYPE = "label_type"
    JSON_BARCODE = "barcode"
    JSON_UPPER_POINT = "upper_point"
    JSON_LOWER_POINT = "lower_point"

    def __init__(self, processingModuleInterface):
        super(ZbarProcessor, self).__init__(processingModuleInterface)

    def process(self, jsonIn):
        """This function receives a json string as parameter and tries decode the barcode
        from the image whose path is inside the json file"""

        # create a json objects with the json string received by parameter
        jsonData = json.loads(jsonIn)

        # find the image inside the json object
        imagePath = str(jsonData[self.JSON_IMAGE])

        # opens the image to process
        img = Image(imagePath)

        # finds barcode data from image
        barcodes = img.findBarcode()

        # create a json output with the image path and the label type received
        jsonOut = {self.JSON_IMAGE: imagePath, self.JSON_LABEL_TYPE: str(jsonData[self.JSON_LABEL_TYPE])}

        # if there is some data processed
        if barcodes is not None:
            # assign the first barcode detected
            barcode = barcodes[0]

            # add the code to jsonOut
            jsonOut[self.JSON_BARCODE] = barcode.data

            # add the top left corner coordinates to jsonOut
            jsonOut[self.JSON_UPPER_POINT] = barcode.topLeftCorner()

            # add the bottom right corner coordinates to jsonOut
            jsonOut[self.JSON_LOWER_POINT] = barcode.bottomRightCorner()

            loadedJson = json.loads(jsonIn)
            jsonOut["deviceID"] = loadedJson["deviceID"]

        # send the jsonIn and jsonOut to the next layer
        self.processingModuleInterface.send(jsonIn, json.dumps(jsonOut))