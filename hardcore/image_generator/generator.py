import os
import sys
from clint.textui import colored, puts

import generator_config_file
from src.processor.automatic_module import AutomaticModule

sys.path.insert(0, os.path.abspath('..'))

def begin():
    res = raw_input("Run all process? (s/n)\n")
    if res == 's' or res == 'S':
        automatic = AutomaticModule()
        puts(colored.blue("Generation started..."))
        automatic.generator(generator_config_file.PATH_ORIGINAL_IMAGES, generator_config_file.PATH_DESTINO)
    else:
        puts(colored.green("Program closed..."))
        sys.exit(0)


pass

begin()
