import os
import platform
from processor.processor import Processor
from processor.automatic_module import AutomaticModule
import sys

sys.path.insert(0, os.path.abspath('..'))

from clint.textui import prompt, puts, colored, validators

res = raw_input("Default Path is : ../XarePoint_Images/Original_Images do you want to change it? (s/n)")
if res == 's':
    # Use a default value and a validator
    path = prompt.query('Location Path', default='../XarePoint_Images/Original_Images/', validators=[validators.PathValidator()])

pathImages = '../XarePoint_Images/Original_Images/'
pathDest = '../XarePoint_Images/'
proc = Processor();

    
def recize():
    height = raw_input("Height: ")
    width = raw_input("Width: ")
    if int(height) > 0 and int(width) > 0:
        proc.ajustDimension(height, width, pathImages, pathDest)
    else:
        puts(colored.red('ERROR: Invalid dimensions!'))
pass


def tumbnail():
    proc.ajustDimension(300, 300, pathImages, pathDest)
pass


def rotate():
    rot = raw_input("Rotation: ")
    if int(rot) > 0:
        proc.rotateImage(rot, pathImages, pathDest)
    else:
        puts(colored.red('ERROR: Invalid rotation!'))
pass


def blur():
    blur = raw_input("Blur: ")
    if int(blur) > 0:
        proc.blurImage(blur, pathImages, pathDest)
    else:
        puts(colored.red('ERROR: Image Blur Invalid!'))
pass


def exit():
    puts(colored.green('You have sucessfuly exited!'))
    sys.exit(0)
pass


def blur_config():
    am = AutomaticModule()
    blur = raw_input("Blur: ")
    if int(blur) > 0:
        if int(blur) > 4:
            puts(colored.yellow('The Blur value is high and the image maybe can be unreadable!'))
        puts(colored.blue("The images will be generated for non Blur and for blur: "+ blur+"!"))
        am.run(blur,pathImages,pathDest)
pass

def begin():
    os.system("cls" if platform.system() == "Windows" else "clear")
    inst = "0"
    while inst is not "x":
        # Shows a list of options to select from
        inst_options = [{'selector':'1', 'prompt':'Recize Image', 'return':'1'},
            {'selector':'2', 'prompt':'Make Tumbnail Image (300x300)', 'return':'2'},
            {'selector':'3', 'prompt':'Rotate Image', 'return':'3'},
            {'selector':'4', 'prompt':'Blur image', 'return':'4'},
            {'selector':'5', 'prompt':'Automatic Generator', 'return':'5'},
            {'selector':'x', 'prompt':'Exit', 'return':'x'}]
        inst = prompt.options("Modify Images:", inst_options)
        options[inst]()        
pass


config_automatic={'1':blur_config,
    'x':begin,    
}

def config_automatic_mode():
    os.system("cls" if platform.system() == "Windows" else "clear")
    config_options = [{'selector':'1', 'prompt':'Blur', 'return':'1'},
        {'selector':'x', 'prompt':'Back', 'return':'x'}]
    
    option = prompt.options("Config Automatic mode: ",config_options)
    
    config_automatic[option]()
pass


options = {'1': recize,
    '2': tumbnail,
    '3': rotate,
    '4': blur,
    '5': config_automatic_mode,
    'x': exit,
}


begin()

    