import Image
import ImageFilter
import os
class Processor(object):
    
    def ajustDimension(self, width, height,pathImages,pathDest):
        self.width = width
        self.height = height
               
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)
                out = im.resize((int(self.width),int(self.height)))
                out.save(pathDest+'AjustDimension/{}_{}{}'.format(fn, self.width, fext))
                print "Image sucessfuly converted in", self.width , "x" , self.height
    pass

    
    def rotateImage(self,rotation,pathImages,pathDest):
        self.rotation = rotation
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)
                out = im.rotate(int(self.rotation),expand=True)
                out.save(pathDest + 'Rotate/{}_{}{}'.format(fn, self.rotation, fext))
                print "Sucess", im
                
        pass
        
        
    def blurImage(self, blur,pathImages,pathDest):
        self.blur = blur
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)
                out = im.convert('L').filter(ImageFilter.GaussianBlur(float(self.blur)))
                out.save(pathDest + 'Blur/{}_{}{}'.format(fn, self.blur, fext))
                print "Sucess: ", im
    pass
    
    def recizeImage(self, fator, pathImages,pathDest):
        self.fator = fator
        for f in os.listdir(pathImages):
            if f.endswith('.jpg'):
                im = Image.open(pathImages + f)
                fn, fext = os.path.splitext(f)
                out = im.resize((image.size[0], image.size[1] / fator),Image.ANTIALIAS)
                out.save(pathDest + 'Recize/{}_{}{}'.format(fn, self.fator, fext))
                print "Sucess: ", im
    pass
        