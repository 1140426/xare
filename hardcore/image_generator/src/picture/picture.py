import Image
import ImageFilter
import json
import os


class Picture(object):
    def __init__(self, pathOrigin, pathDest, f, saveFolder):
        self.image = Image.open(pathOrigin + f)
        self.imageName, self.imageExtension = os.path.splitext(f)
        self.pathOrigin = pathOrigin
        self.pathDest = pathDest
        self.saveFolder = saveFolder
        self.originalName = self.imageName
        self.blur = 0
        self.rotation = 0
        self.transposed = False

    pass

    def getName(self):
        return self.imageName

    pass

    def applyBlur(self, blur):
        im = self.image.convert('L').filter(ImageFilter.GaussianBlur(float(blur)))
        self.blur = blur
        self.image = im
        self.save(self.originalName, "{}_B{}{}".format(self.imageName, self.blur, self.imageExtension))

    pass

    def rotateImage(self, rotation):
        im = self.image.rotate(int(rotation), expand=True)
        self.rotation = rotation
        self.image = im
        self.save(self.originalName,
                  "{}_B{}_R{}{}".format(self.imageName, self.blur, self.rotation, self.imageExtension))

    pass

    def transposeImage(self):
        im = self.image.transpose(Image.FLIP_LEFT_RIGHT)
        self.transposed = True
        self.image = im
        self.save(self.originalName, "{}_T{}{}".format(self.imageName, self.blur, self.imageExtension))

    pass

    def save(self, originalName, name):
        self.image.save(self.pathDest + self.saveFolder + name + self.imageExtension)

        with open(self.pathOrigin + originalName + '.json', 'r+') as f:
            data = json.load(f)
            f.close()

        data["name"] = self.imageName + self.imageExtension
        data["blur"] = str(self.blur)
        data["rotation"] = str(self.rotation)
        data["transposed"] = str(self.transposed)

        jsonFile = open(self.pathDest + self.saveFolder + str(self.imageName) + ".json", "w+")
        jsonFile.write(json.dumps(data))
        jsonFile.close()

    pass

    def saveOriginal(self):
        self.save(self.originalName, self.imageName)

    pass
