/*
								Received RabbitMQ message Service

	Deals with a received message from RabbitMQ and it is called by 'RabbitController'.
*/
angular.module('starter').service("receivedRabbitMQservice", function(){


	//displays the received message in the screen
	this.displayReceivedMessage = function(queueName){

		//connect to the web socket and subscribe a queue
		var client = connectToWebSocket();

		var on_connect = function() {
		  client.subscribe("/queue/"+ queueName, on_message);

		  console.log('connected');
		};

		var on_error =  function() {
		  console.log('error with releaseAllRabbits');
		};


		client.connect('guest', 'guest', on_connect, on_error, '/');


		var on_message = function(message) {
			console.log('I\'ve received a message!');
			
			var formattedMessage = formatMessageAsHTML(message.body);

			document.getElementById("contentDiv").innerHTML = formattedMessage;
		}
		
	};


//-------------------------------------------------------------------------------------------------------------------

	//private functions used to connect to the web socket
	function connectToWebSocket(){
		var ws = new SockJS('http://192.168.4.235:5001/stomp');
		var client = Stomp.over(ws);

		return client;
	};

	//formats the received message as HTML
	function formatMessageAsHTML(content){
		var formattedMessage = "<h2><br>" + content + "</h2>";

		return formattedMessage;
	};

});

/*
				TODO

	- pass url as argument
	- pass binding key as argument
	- pass log in data as argument
*/
