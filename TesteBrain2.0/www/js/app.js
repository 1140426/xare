angular.module('starter', ['ionic', 'ionicTitle', 'ngCordova','ionicResult'])

  .run(function ($ionicPlatform,receivedRabbitMQservice,$cordovaDevice) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(false);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

        var deviceId=$cordovaDevice.getUUID();
        console.log(deviceId);
        var deviceId = "b254da5888cadf31";

      receivedRabbitMQservice.displayReceivedMessage(deviceId);
    });


  })

  .config(function ($stateProvider, $urlRouterProvider) {



    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })

      // Each tab has its own nav history stack:

        .state('tab.title', {
            url: '/title',
            views: {
                'tab-title': {
                    templateUrl:'templates/tab-title.html',
                    controller: 'titleCtrl'
                }
            }
      })

      .state('tab.result', {
          url: '/result',
          views: {
              'tab-result': {
                  templateUrl:'templates/tab-result.html',
                  controller: 'resultCtrl'
              }
          }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/result');

  });
